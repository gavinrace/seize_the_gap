// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.35 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.35;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.8278546,fgcg:0.9174322,fgcb:0.9705882,fgca:1,fgde:0.015,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:9361,x:33209,y:32712,varname:node_9361,prsc:2|normal-3835-OUT,emission-8596-OUT,custl-5085-OUT;n:type:ShaderForge.SFN_LightAttenuation,id:8068,x:32491,y:33488,varname:node_8068,prsc:2;n:type:ShaderForge.SFN_LightColor,id:3406,x:32491,y:33354,varname:node_3406,prsc:2;n:type:ShaderForge.SFN_LightVector,id:6869,x:30976,y:32609,varname:node_6869,prsc:2;n:type:ShaderForge.SFN_NormalVector,id:9684,x:30976,y:32737,prsc:2,pt:True;n:type:ShaderForge.SFN_HalfVector,id:9471,x:31485,y:32907,varname:node_9471,prsc:2;n:type:ShaderForge.SFN_Dot,id:7782,x:31188,y:32652,cmnt:Lambert,varname:node_7782,prsc:2,dt:1|A-6869-OUT,B-9684-OUT;n:type:ShaderForge.SFN_Dot,id:3269,x:31697,y:32845,cmnt:Blinn-Phong,varname:node_3269,prsc:2,dt:1|A-9684-OUT,B-9471-OUT;n:type:ShaderForge.SFN_Multiply,id:2746,x:32092,y:32855,cmnt:Specular Contribution,varname:node_2746,prsc:2|A-148-OUT,B-5267-OUT,C-5231-OUT;n:type:ShaderForge.SFN_Multiply,id:1941,x:32058,y:32675,cmnt:Diffuse Contribution,varname:node_1941,prsc:2|A-1035-OUT,B-148-OUT;n:type:ShaderForge.SFN_Color,id:5927,x:31701,y:32509,ptovrint:False,ptlb:Snow Color,ptin:_SnowColor,varname:node_5927,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Exp,id:1700,x:31697,y:33046,varname:node_1700,prsc:2,et:1|IN-9978-OUT;n:type:ShaderForge.SFN_Slider,id:5328,x:31125,y:33231,ptovrint:False,ptlb:Gloss,ptin:_Gloss,varname:node_5328,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Power,id:5267,x:31895,y:32914,varname:node_5267,prsc:2|VAL-3269-OUT,EXP-1700-OUT;n:type:ShaderForge.SFN_Add,id:2159,x:32394,y:32816,cmnt:Combine,varname:node_2159,prsc:2|A-1941-OUT,B-2746-OUT;n:type:ShaderForge.SFN_Multiply,id:5085,x:32687,y:33329,cmnt:Attenuate and Color,varname:node_5085,prsc:2|A-2159-OUT,B-3406-RGB,C-8068-OUT;n:type:ShaderForge.SFN_ConstantLerp,id:9978,x:31485,y:33077,varname:node_9978,prsc:2,a:1,b:11|IN-5328-OUT;n:type:ShaderForge.SFN_AmbientLight,id:7528,x:32443,y:32669,varname:node_7528,prsc:2;n:type:ShaderForge.SFN_Multiply,id:2460,x:32681,y:32496,cmnt:Ambient Light,varname:node_2460,prsc:2|A-1035-OUT,B-7528-RGB;n:type:ShaderForge.SFN_NormalVector,id:2964,x:31478,y:32088,prsc:2,pt:False;n:type:ShaderForge.SFN_ComponentMask,id:177,x:31660,y:32088,varname:node_177,prsc:2,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-2964-OUT;n:type:ShaderForge.SFN_Clamp01,id:7304,x:31841,y:32088,varname:node_7304,prsc:2|IN-177-G;n:type:ShaderForge.SFN_Lerp,id:1035,x:32349,y:32448,varname:node_1035,prsc:2|A-5927-RGB,B-9143-RGB,T-492-OUT;n:type:ShaderForge.SFN_Step,id:5309,x:32167,y:32064,varname:node_5309,prsc:2|A-7304-OUT,B-193-OUT;n:type:ShaderForge.SFN_Vector1,id:193,x:32018,y:32137,varname:node_193,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Slider,id:7166,x:32313,y:32271,ptovrint:False,ptlb:Snow Amount,ptin:_SnowAmount,varname:node_7166,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:0.75;n:type:ShaderForge.SFN_Vector3,id:4178,x:32470,y:32118,varname:node_4178,prsc:2,v1:1,v2:1,v3:1;n:type:ShaderForge.SFN_Lerp,id:492,x:32661,y:32225,varname:node_492,prsc:2|A-4178-OUT,B-5309-OUT,T-7166-OUT;n:type:ShaderForge.SFN_Color,id:9143,x:31701,y:32341,ptovrint:False,ptlb:Main Color,ptin:_MainColor,varname:node_9143,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Tex2d,id:1351,x:32867,y:31864,ptovrint:False,ptlb:Snow Normal,ptin:_SnowNormal,varname:node_1351,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:2a2eb704d4d9e8f4b807ea360c9bc16d,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Lerp,id:3835,x:33111,y:32208,varname:node_3835,prsc:2|A-1351-RGB,B-8397-RGB,T-492-OUT;n:type:ShaderForge.SFN_Tex2d,id:8397,x:32867,y:32060,ptovrint:False,ptlb:Default Normal,ptin:_DefaultNormal,varname:node_8397,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:b7649e64d087c4f6297faa32dbbbf133,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Vector3,id:2706,x:31670,y:33421,varname:node_2706,prsc:2,v1:0,v2:0,v3:0;n:type:ShaderForge.SFN_Vector3,id:5915,x:31559,y:33319,varname:node_5915,prsc:2,v1:1,v2:1,v3:1;n:type:ShaderForge.SFN_Lerp,id:5231,x:31874,y:33282,varname:node_5231,prsc:2|A-2706-OUT,B-5915-OUT,T-5328-OUT;n:type:ShaderForge.SFN_Fresnel,id:5366,x:32619,y:32931,varname:node_5366,prsc:2|EXP-5328-OUT;n:type:ShaderForge.SFN_Multiply,id:8596,x:32937,y:32481,varname:node_8596,prsc:2|A-2460-OUT,B-7052-OUT;n:type:ShaderForge.SFN_Multiply,id:745,x:32915,y:32866,varname:node_745,prsc:2|A-5366-OUT,B-8677-RGB;n:type:ShaderForge.SFN_Cubemap,id:8677,x:32832,y:33045,ptovrint:False,ptlb:CubeMap,ptin:_CubeMap,varname:node_8677,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,cube:28232cc4f9642423a80c21bcc071b296,pvfc:-1;n:type:ShaderForge.SFN_Lerp,id:7052,x:32915,y:32649,varname:node_7052,prsc:2|A-745-OUT,B-9904-OUT,T-6903-OUT;n:type:ShaderForge.SFN_Vector1,id:9904,x:32601,y:32768,varname:node_9904,prsc:2,v1:0;n:type:ShaderForge.SFN_OneMinus,id:6903,x:32712,y:32639,varname:node_6903,prsc:2|IN-5328-OUT;n:type:ShaderForge.SFN_LightVector,id:6097,x:30035,y:32257,cmnt:Cel shading,varname:node_6097,prsc:2;n:type:ShaderForge.SFN_NormalVector,id:3798,x:30035,y:32424,prsc:2,pt:True;n:type:ShaderForge.SFN_Dot,id:337,x:30221,y:32329,varname:node_337,prsc:2,dt:1|A-6097-OUT,B-3798-OUT;n:type:ShaderForge.SFN_Power,id:9362,x:30485,y:32346,varname:node_9362,prsc:2|VAL-337-OUT,EXP-8306-OUT;n:type:ShaderForge.SFN_Power,id:6009,x:30671,y:32346,varname:node_6009,prsc:2|VAL-9362-OUT,EXP-4230-OUT;n:type:ShaderForge.SFN_ValueProperty,id:8306,x:30313,y:32510,ptovrint:False,ptlb:GradientSize,ptin:_GradientSize,varname:node_5783,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2;n:type:ShaderForge.SFN_ValueProperty,id:4230,x:30512,y:32539,ptovrint:False,ptlb:GradientSharpness,ptin:_GradientSharpness,varname:node_1399,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2;n:type:ShaderForge.SFN_OneMinus,id:5003,x:30862,y:32346,varname:node_5003,prsc:2|IN-6009-OUT;n:type:ShaderForge.SFN_Power,id:4224,x:31070,y:32346,varname:node_4224,prsc:2|VAL-5003-OUT,EXP-6681-OUT;n:type:ShaderForge.SFN_Exp,id:6681,x:30751,y:32517,varname:node_6681,prsc:2,et:0|IN-4230-OUT;n:type:ShaderForge.SFN_OneMinus,id:2487,x:31270,y:32346,varname:node_2487,prsc:2|IN-4224-OUT;n:type:ShaderForge.SFN_Clamp01,id:148,x:31450,y:32346,varname:node_148,prsc:2|IN-2487-OUT;proporder:5927-5328-7166-9143-1351-8397-8677-8306-4230;pass:END;sub:END;*/

Shader "Shader Forge/SimpleSnow" {
    Properties {
        _SnowColor ("Snow Color", Color) = (1,1,1,1)
        _Gloss ("Gloss", Range(0, 1)) = 0
        _SnowAmount ("Snow Amount", Range(0, 0.75)) = 0
        _MainColor ("Main Color", Color) = (1,0,0,1)
        _SnowNormal ("Snow Normal", 2D) = "bump" {}
        _DefaultNormal ("Default Normal", 2D) = "bump" {}
        _CubeMap ("CubeMap", Cube) = "_Skybox" {}
        _GradientSize ("GradientSize", Float ) = 2
        _GradientSharpness ("GradientSharpness", Float ) = 2
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _SnowColor;
            uniform float _Gloss;
            uniform float _SnowAmount;
            uniform float4 _MainColor;
            uniform sampler2D _SnowNormal; uniform float4 _SnowNormal_ST;
            uniform sampler2D _DefaultNormal; uniform float4 _DefaultNormal_ST;
            uniform samplerCUBE _CubeMap;
            uniform float _GradientSize;
            uniform float _GradientSharpness;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
                UNITY_FOG_COORDS(7)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos(v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _SnowNormal_var = UnpackNormal(tex2D(_SnowNormal,TRANSFORM_TEX(i.uv0, _SnowNormal)));
                float3 _DefaultNormal_var = UnpackNormal(tex2D(_DefaultNormal,TRANSFORM_TEX(i.uv0, _DefaultNormal)));
                float node_5309 = step(saturate(i.normalDir.rg.g),0.5);
                float3 node_492 = lerp(float3(1,1,1),float3(node_5309,node_5309,node_5309),_SnowAmount);
                float3 normalLocal = lerp(_SnowNormal_var.rgb,_DefaultNormal_var.rgb,node_492);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
////// Emissive:
                float3 node_1035 = lerp(_SnowColor.rgb,_MainColor.rgb,node_492);
                float node_9904 = 0.0;
                float3 emissive = ((node_1035*UNITY_LIGHTMODEL_AMBIENT.rgb)*lerp((pow(1.0-max(0,dot(normalDirection, viewDirection)),_Gloss)*texCUBE(_CubeMap,viewReflectDirection).rgb),float3(node_9904,node_9904,node_9904),(1.0 - _Gloss)));
                float node_148 = saturate((1.0 - pow((1.0 - pow(pow(max(0,dot(lightDirection,normalDirection)),_GradientSize),_GradientSharpness)),exp(_GradientSharpness))));
                float3 finalColor = emissive + (((node_1035*node_148)+(node_148*pow(max(0,dot(normalDirection,halfDirection)),exp2(lerp(1,11,_Gloss)))*lerp(float3(0,0,0),float3(1,1,1),_Gloss)))*_LightColor0.rgb*attenuation);
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _SnowColor;
            uniform float _Gloss;
            uniform float _SnowAmount;
            uniform float4 _MainColor;
            uniform sampler2D _SnowNormal; uniform float4 _SnowNormal_ST;
            uniform sampler2D _DefaultNormal; uniform float4 _DefaultNormal_ST;
            uniform samplerCUBE _CubeMap;
            uniform float _GradientSize;
            uniform float _GradientSharpness;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
                UNITY_FOG_COORDS(7)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos(v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _SnowNormal_var = UnpackNormal(tex2D(_SnowNormal,TRANSFORM_TEX(i.uv0, _SnowNormal)));
                float3 _DefaultNormal_var = UnpackNormal(tex2D(_DefaultNormal,TRANSFORM_TEX(i.uv0, _DefaultNormal)));
                float node_5309 = step(saturate(i.normalDir.rg.g),0.5);
                float3 node_492 = lerp(float3(1,1,1),float3(node_5309,node_5309,node_5309),_SnowAmount);
                float3 normalLocal = lerp(_SnowNormal_var.rgb,_DefaultNormal_var.rgb,node_492);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 node_1035 = lerp(_SnowColor.rgb,_MainColor.rgb,node_492);
                float node_148 = saturate((1.0 - pow((1.0 - pow(pow(max(0,dot(lightDirection,normalDirection)),_GradientSize),_GradientSharpness)),exp(_GradientSharpness))));
                float3 finalColor = (((node_1035*node_148)+(node_148*pow(max(0,dot(normalDirection,halfDirection)),exp2(lerp(1,11,_Gloss)))*lerp(float3(0,0,0),float3(1,1,1),_Gloss)))*_LightColor0.rgb*attenuation);
                fixed4 finalRGBA = fixed4(finalColor * 1,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
