﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VehicleModHandler : MonoBehaviour {


	/* 
	PurchasableItem currentlySelectedMainColor;
	PurchasableItem currentlySelectedAccentColor;
	PurchasableItem currentlySelectedTopper;
	PurchasableItem currentlySelectedAccessory;

	public int currentTotalPrice;
	public Text currentPriceText;
	*/
	public bool inMenu;
	public VehicleModParams currentParams;
	public Transform playerHandle;
	/*
	[Header("Vehicle Settings")]
	public int currentCar;
	public List<int> cars;
	*/
	[Header("Vehicle")]
	public GameObject vehicleObj;
	public int vehicleIndex;
	[Header("Color Changes")]
	//public int mainColorIndex;
	//public int accentColorIndex;
	/*
	public int currentMainColor;
	public int currentAccentColor;
	public List<Color> availableMainColors;
	public List<Color> availableAccentColors;*/
	public VertexColorChooser mainMesh;
	public VertexColorChooser accentMesh;/*
	public List<int> mainColorPriceList;
	*/
	[Header("Add-ons")]
	public GameObject topperObj;
	public Transform topperPos;/*
	public int currentTopper;
	public List<GameObject> availableToppers;*/
	public GameObject accessoryObj;
	GameData currentData;
	/*public Transform accessoryPos;
	public int currentAccessory;
	public List<GameObject> availableAccessories;

	[Header("Lists of Puchasable Items")]
	public List<PurchasableItem> mainColorsPI;
	public List<PurchasableItem> accentColorsPI;
	public List<PurchasableItem> toppersPI;

	[Header("Scroll Views")]
	public Transform mainColorMenuContent;
	public Transform accentColorMenuContent;
	public Transform topperMenuContent;
	public GameObject genericButton;*/

	private void Start()
	{
		/*
		currentlySelectedMainColor = gameObject.AddComponent<PurchasableItem>();
		currentlySelectedAccentColor = gameObject.AddComponent<PurchasableItem>();
		currentlySelectedTopper = gameObject.AddComponent<PurchasableItem>();
		currentlySelectedAccessory = gameObject.AddComponent<PurchasableItem>();

		ResetCurrentlySelected();*/

		Invoke("LoadConfig", 0.1f);
	}

	public void LoadConfig()
	{
		


		currentData = FileManager.instance.currentGameData;

		if (inMenu)
		{
			//if we are in the mnenu, let the garage handle everything
			//sync currently installed mods to garage
			GarageManager.instance.LoadConfigVehicle(currentData);
			LoadCurrentCar();

		}
		else
		{
			//if we are in the main level, just spawn the saved configuration
			LoadCurrentCar();

		}

	}

	 void LoadCurrentCar()
	{
		SpawnVehicle(currentData.playerVehicle);

		AssignParams(vehicleObj.GetComponent<VehicleModParams>());

		if (!inMenu) 
		{
			//update speed and strength of selected car
			GameManager.instance.player.agility = currentParams.steering;
			GameManager.instance.player.strength = currentParams.strength;
		}

		print("loaded main color = " + currentData.playerCarMainColor);
		ChangeMainColor(currentData.playerCarMainColor);
		ChangeAccentColor(currentData.playerCarAccentColor);
		InstallTopper(currentData.playerTopper);
	}

	public void AssignParams (VehicleModParams curParams)
	{
		currentParams = curParams;

		//mainColorIndex = currentParams.mainColorIndex;
		//accentColorIndex = currentParams.accentColorIndex;
		mainMesh = currentParams.mainMesh;
		accentMesh = currentParams.accentMesh;
		topperPos = currentParams.topperPos;
	}

	void SpawnVehicle(int item)
	{
		if (vehicleObj != null)
		{
			Destroy(vehicleObj);
		}
		vehicleObj = GameObject.Instantiate(ModHolder.instance.availableVehicles[item], playerHandle.position, playerHandle.rotation, playerHandle) as GameObject;

	}

	void ChangeMainColor(int item)
	{
		//print("renderer obj = " + rendererMat.gameObject);
		mainMesh.ChangeColor(ModHolder.instance.availableMainColors[item]);
		//currentMainColor = item;	
	}

	void ChangeAccentColor(int item)
	{
		accentMesh.ChangeColor(ModHolder.instance.availableAccentColors[item]);
		//currentAccentColor = item;	
	}

	void InstallTopper (int item)
	{
		GameObject.Destroy(topperObj);
		topperObj = GameObject.Instantiate(ModHolder.instance.availableToppers[item],topperPos.position, topperPos.rotation, topperPos) as GameObject;
		//currentTopper = item;
	}
}
