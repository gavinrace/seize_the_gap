﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingSpawner : MonoBehaviour
{


	public List<GameObject> small;
	public List<GameObject> medium;
	public List<GameObject> large;

	public List<GameObject> smallPool;
	public List<GameObject> mediumPool;
	public List<GameObject> largePool;

	float weightLow = 0f;
	float weightHigh = 30f;
	public int weightMax = 100;
	public float weightIncrement;

	public float offsetMin;

	//public string objTag;
	public float spawnOffset;

	//small edit to file

	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "BuildingEnd")
		{

			SpawnObj();

		}
	}

	void IncrementBuildingPorperties()
	{
		if (weightHigh < weightMax)
		{
			weightHigh += weightIncrement;
			
		}

		if (weightLow < weightHigh - 25)
		{
			weightLow += weightIncrement / 1.5f;
		}

		if (spawnOffset >= offsetMin)
		{
			spawnOffset -= 0.1f;
		}

	}

	void SpawnObj()
	{
		GameObject newObj;

		//check random number within current weight settings
		int randNum;

		randNum = Mathf.RoundToInt(Random.Range(weightLow, weightHigh));

		if (randNum < 33)
		{
			newObj = GetPooledSmall();
		}
		else if (randNum > 33 && randNum < 66)
		{
			newObj = GetPooledMedium();
		}
		else if (randNum > 66)
		{
			newObj = GetPooledLarge();
		}
		else
		{
			//failsafe
			newObj = GetPooledMedium();
		}

		newObj.transform.position = new Vector3(transform.position.x, 0, transform.position.z + spawnOffset);

		Transform model = newObj.transform.GetChild(0);
		model.forward = transform.forward;

		IncrementBuildingPorperties();
	}

	GameObject GetPooledSmall()
	{
		for (int i = 0; i < smallPool.Count; i++)
		{
			if (smallPool[i].activeInHierarchy == false)
			{
				//print("found inactive car to use");
				smallPool[i].SetActive(true);
				return smallPool[i];
			}
			else
			{
				//print("car is active");
			}
		}
		//print("went through whole list and found no inactive cars");
		//none left
		GameObject newSmallObj = GameObject.Instantiate(small[Random.Range(0, small.Count)]);
		smallPool.Add(newSmallObj);
		MovementManager.instance.movingObjects.Add(newSmallObj);
		return newSmallObj;
	}

	GameObject GetPooledMedium()
	{
		for (int i = 0; i < mediumPool.Count; i++)
		{
			if (mediumPool[i].activeInHierarchy == false)
			{
				//print("found inactive car to use");
				mediumPool[i].SetActive(true);
				return mediumPool[i];
			}
			else
			{
				//print("car is active");
			}
		}
		//print("went through whole list and found no inactive cars");
		//none left
		GameObject newMedObj = GameObject.Instantiate(medium[Random.Range(0, medium.Count)]);
		mediumPool.Add(newMedObj);
		MovementManager.instance.movingObjects.Add(newMedObj);
		return newMedObj;
	}

	GameObject GetPooledLarge()
	{
		for (int i = 0; i < largePool.Count; i++)
		{
			if (largePool[i].activeInHierarchy == false)
			{
				//print("found inactive car to use");
				largePool[i].SetActive(true);
				return largePool[i];
			}
			else
			{
				//print("car is active");
			}
		}
		//print("went through whole list and found no inactive cars");
		//none left
		GameObject newLargeObj = GameObject.Instantiate(large[Random.Range(0, large.Count)]);
		largePool.Add(newLargeObj);
		MovementManager.instance.movingObjects.Add(newLargeObj);
		return newLargeObj;
	}


}
