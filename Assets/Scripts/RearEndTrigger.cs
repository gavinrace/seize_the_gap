﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RearEndTrigger : MonoBehaviour {

	void OnTriggerEnter (Collider col)
	{
		if (col.gameObject.tag == "NPC")
		{
			print("Hit front bumper on " + col.gameObject);
			gameObject.transform.parent.parent.parent.GetComponent<CarManager>().Crash();
		}
	}
}
