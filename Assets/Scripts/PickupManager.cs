﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupManager : MonoBehaviour
{

	public GameObject health;
	public GameObject powerup1;
	public GameObject powerup2;
	public GameObject powerup3;
	public List<GameObject> healthPool;
	public List<GameObject> powerup1Pool;
	public List<GameObject> powerup2Pool;
	public List<GameObject> powerup3Pool;
	public float minDelay;
	public float maxDelay;

	public bool gameOver;


	// Use this for initialization
	void Start()
	{
		StartCoroutine(SpawnPickup());
	}

	IEnumerator SpawnPickup()
	{
		while (!gameOver)
		{
			float delay = Random.Range(minDelay, maxDelay);
			yield return new WaitForSeconds(delay);

			int lanePos = Random.Range(0, 3);

			GameObject pickup = GetPooledPointObj(lanePos);
			pickup.transform.position = new Vector3 (TrafficManager.instance.lanes[lanePos].position.x, 0, 200);
		}
	}

	GameObject GetPooledPointObj(int dir)
	{

		int ranNum = Random.Range(0, 100);
		/*switch (dir)
		{
			
			case 0:
				//in first lane from the left. Supports any arrangement except one that goes left.
				if ( ranNum < 33)
				{
					return PooledStraight();
				}
				else if (ranNum > 33 && ranNum < 66)
				{
					return PooledRight();
				}
				else
				{
					return PooledSingle();
				}

			case 1:
				//in one of the middle lanes. Anything will work.
				if (ranNum < 25)
				{
					return PooledStraight();
				}
				else if (ranNum > 25 && ranNum < 50)
				{
					return PooledRight();
				}
				else if (ranNum > 50 && ranNum < 75)
				{
					return PooledLeft();
				}
				else
				{
					return PooledSingle();
				}
			case 2:
				//in one of the middle lanes. Anything will work.
				if (ranNum < 25)
				{
					return PooledStraight();
				}
				else if (ranNum > 25 && ranNum < 50)
				{
					return PooledRight();
				}
				else if (ranNum > 50 && ranNum < 75)
				{
					return PooledLeft();
				}
				else
				{
					return PooledSingle();
				}

			case 3:
				//in far right lanes.Supports anything but right.
				if (ranNum < 33)
				{
					return PooledStraight();
				}
				else if (ranNum > 33 && ranNum < 66)
				{
					return PooledLeft();
				}
				else
				{
					return PooledSingle();
				}

			default:
				//this should never ever happen
				return new GameObject();
		}*/

		if (ranNum < 15)
		{
			//spawn health
			return GetPooledHealth();
		}

		else if (ranNum > 15 && ranNum < 45)
		{
			//spawn powerup 1
			return GetPooledPowerup1();
		}
		else if (ranNum > 45 && ranNum < 75)
		{
			//spawn powerup 2
			return GetPooledPowerup2();
		}
		else
		{
			//spawn powerup 3
			return GetPooledPowerup3();
		}


	}

	GameObject GetPooledHealth()
	{
		for (int i = 0; i < healthPool.Count; i++)
		{
			if (healthPool[i].activeInHierarchy == false)
			{
				//print("found inactive point obj to use");
				healthPool[i].SetActive(true);
				return healthPool[i];
			}
			else
			{
				//print("point obj is active");
			}
		}
		//print("went through whole list and found no inactive point objs);
		//none left
		GameObject newHealthObj = GameObject.Instantiate(health);
		healthPool.Add(newHealthObj);
		MovementManager.instance.movingObjects.Add(newHealthObj);
		return newHealthObj;
	}
	GameObject GetPooledPowerup1()
	{
		for (int i = 0; i < powerup1Pool.Count; i++)
		{
			if (powerup1Pool[i].activeInHierarchy == false)
			{
				//print("found inactive point obj to use");
				powerup1Pool[i].SetActive(true);
				return powerup1Pool[i];
			}
			else
			{
				//print("point obj is active");
			}
		}
		//print("went through whole list and found no inactive point objs);
		//none left
		GameObject newObj = GameObject.Instantiate(powerup1);
		powerup1Pool.Add(newObj);
		MovementManager.instance.movingObjects.Add(newObj);
		return newObj;
	}

	GameObject GetPooledPowerup2()
	{
		for (int i = 0; i < powerup2Pool.Count; i++)
		{
			if (powerup2Pool[i].activeInHierarchy == false)
			{
				//print("found inactive point obj to use");
				powerup2Pool[i].SetActive(true);
				return powerup2Pool[i];
			}
			else
			{
				//print("point obj is active");
			}
		}
		//print("went through whole list and found no inactive point objs);
		//none left
		GameObject newObj = GameObject.Instantiate(powerup2);
		powerup2Pool.Add(newObj);
		MovementManager.instance.movingObjects.Add(newObj);
		return newObj;
	}

	GameObject GetPooledPowerup3()
	{
		for (int i = 0; i < powerup3Pool.Count; i++)
		{
			if (powerup3Pool[i].activeInHierarchy == false)
			{
				//print("found inactive point obj to use");
				powerup3Pool[i].SetActive(true);
				return powerup3Pool[i];
			}
			else
			{
				//print("point obj is active");
			}
		}
		//print("went through whole list and found no inactive point objs);
		//none left
		GameObject newObj = GameObject.Instantiate(powerup3);
		powerup3Pool.Add(newObj);
		MovementManager.instance.movingObjects.Add(newObj);
		return newObj;
	}


}
