﻿using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections.Generic;

public class FileManager : MonoBehaviour {

	public static FileManager instance;
	public GameData currentGameData;

	private int stylePoints;

	private void Start()
	{
		instance = this;
		Invoke("Load", 0.01f);
	}

	public void Load()
	{
		if (File.Exists(Application.persistentDataPath + "/persistant.data"))
		{
			print("Found file at " + Application.persistentDataPath + "/persistant.data");
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/persistant.data", FileMode.Open);

			GameData loadedData = new GameData();
			loadedData = bf.Deserialize(file) as GameData;

			file.Close();

			currentGameData = loadedData;

			print("Should've loaded SP: " + currentGameData.stylePoints + " CL: " + currentGameData.playerChallengeList + " MCL: " + currentGameData.masterChallengeList);
		}
		else
		{
			
			FullReset();
			
		}

		stylePoints = currentGameData.stylePoints;

		//now that it's loaded, begin parsing the stats in Stat Tracker
		StatTracker.instance.ParseChallenges(currentGameData.playerChallengeList);
		StylePointHandler.instance.SyncLoadedStylePoints();

		print("Loaded frame rate = " + currentGameData.frameRate);
	}

	public void FullReset()
	{
		//no file exists so we need to create a new empty one
		currentGameData = new GameData();
		currentGameData.maxSavedSP = 0;
		currentGameData.playerCarAccentColor = 0;
		currentGameData.playerCarMainColor = 0;
		currentGameData.playerTopper = 0;
		currentGameData.playerVehicle = 0;
		currentGameData.stylePoints = 0;
		currentGameData.boughtHealth = false;
		currentGameData.boughtHorn = false;
		currentGameData.boughtMoneyStack = false;
		currentGameData.boughtTires = false;
		currentGameData.maxDistance = 0;
		currentGameData.frameRate = 30;
		currentGameData.purchasedvehicles = new List<int>();

		currentGameData.playerChallengeList = currentGameData.masterChallengeList;

		Save(currentGameData);
	}

	public int ReturnLoadedStylePoints()
	{
		return stylePoints;
	}

	public void Save(GameData data)
	{
		BinaryFormatter binFormatter = new BinaryFormatter();
		FileStream file = File.Create(Application.persistentDataPath + "/persistant.data");

		binFormatter.Serialize(file, data);
		file.Close();
		currentGameData = data;
		print("Should've saved SP: " + data.stylePoints + " CL: " + data.playerChallengeList + " MCL: " + data.masterChallengeList);
		print("saved max data of " + data.maxDistance);
	}
}


