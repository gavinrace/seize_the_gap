﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuSceneLoader : MonoBehaviour {

	public List<string> Levels;
	public bool inLevel;

	public void LoadMainLevel()
	{
		print("Load main Level");
		StartCoroutine(LevelTransition(Levels[1]));
	}

	public void LoadMainMenu()
	{
		print("Load main Menu");
		StartCoroutine(LevelTransition(Levels[0]));
	}

	public void LoadCustomizer()
	{
		print("Load Customize Menu");
		StartCoroutine(LevelTransition(Levels[2]));
	}

	IEnumerator LevelTransition(string level)
	{
		if (inLevel)
		{
			if (GameManager.instance.paused) 
			{
				GameManager.instance.PauseGame();
			}

			GameManager.instance.ToggleMenu(false);
		}

		GetComponent<ScreenFader>().FadeScreen(false);
		yield return new WaitForSeconds(1f);
		GetComponent<Camera>().cullingMask = -2;
		GetComponent<Camera>().clearFlags = CameraClearFlags.SolidColor;

		print("Load " + level);
		SceneManager.LoadScene(level, LoadSceneMode.Single);

	}
}
