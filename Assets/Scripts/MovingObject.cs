﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObject : MonoBehaviour {

	float movementSpeed;
	// Use this for initialization
	void OnEnable () {

		Invoke("FindMM", 0.2f);
		print("This is on obj: " + gameObject);

	}

	void FindMM ()
	{
		movementSpeed = MovementManager.instance.movementSpeed;
	}

	void Update ()
	{
		transform.Translate(-transform.forward * Time.deltaTime * movementSpeed);
	}


}
