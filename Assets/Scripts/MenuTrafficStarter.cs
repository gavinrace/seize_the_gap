﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuTrafficStarter : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine(StartMenuTraffic());
	}

	IEnumerator StartMenuTraffic ()
	{
		yield return new WaitForSeconds(0.1f);
		GetComponent<TrafficManager>().PubStartSpawn();
	}

}
