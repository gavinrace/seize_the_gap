﻿using UnityEngine;
using UnityEngine.UI;

public class TiresManager : MonoBehaviour {

	private CarManager player;
	public int tireProtection;
	public ParticleSystem leftTireParticle;
	public ParticleSystem rightTireParticle;
	public Text tireText;

	// Use this for initialization
	void Start () {
		player = GetComponent<CarManager>();
	}
	
	public void HitPothole (int damage, bool side)
	{
		if (tireProtection > 0)
		{
			tireProtection--;
			UsedUpProtection(side);
			UpdateText();

			//check if we've used the last one and disable text
			if (tireProtection == 0)
			{
				tireText.gameObject.SetActive(false);
			}
		}
		else
		{
			player.DoDamage(damage);
			GetComponent<Animator>().SetTrigger("HitObject");
		}


	}

	public void PickedUpProtection ()
	{
		//hard coding it because 4 tires.
		tireProtection = 4;
		tireText.gameObject.SetActive(true);
		UpdateText();

	}

	void UpdateText ()
	{
		tireText.text = tireProtection.ToString();
	}

	void UsedUpProtection (bool side)
	{
		//use this to visualize losing an all-weather tire
		//left is true
		//right is false

		if (side)
		{
			leftTireParticle.Emit(1);
		}
		else
		{
			rightTireParticle.Emit(1);
		}

	}
}
