﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CarManager : MonoBehaviour {

	[Header("Vehicle Specific Stats")]
	public float agility;
	public float strength = 1;
	public int sideswipeDamage;
	bool canBeDamaged = true;
	public float invulnTime;

	bool touchedLeft;
	bool touchedRight;
	int touchPos;

	public int potholeCounter;
	public int health;
	public Text healthText;

	public bool crashed;
	public bool otherSide;

	private Rigidbody rb;

	public Image healthBar;
	private float healthBarIncrements;
	public Color healthColor;
	public Color greenColor;
	public Color redColor;

	//public Text debugCurrentSpeed;
	//public Text debugCarSpeed;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody>();
		//StartCoroutine(GroundChecker());

		//get orig width of health bar
		healthBarIncrements = healthBar.rectTransform.sizeDelta.x / 100;

		//Invoke("DisplaySpeed",1);

	}

	/*void DisplaySpeed()
	{
		debugCarSpeed.text = "Car Speed: " + speed;
	}*/
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		if (!crashed)
		{
			//float touchPos;
			float halfScreen = Screen.width / 2;

			if (Input.GetAxis("Horizontal") > 0)
			{
				/*touchedRight = true;
				touchedLeft = false;
				*/

				touchPos = 1;
				//rb.AddForce(Input.GetAxis("Horizontal") * speed /** Time.fixedDeltaTime*/, 0, 0);
				//debugCurrentSpeed.text = "CurrentSpeed = " + (Input.GetAxis("Horizontal") * speed).ToString();;
			}
			else if (Input.GetAxis("Horizontal") < 0) {
				/*touchedLeft = true;
				touchedRight = false;*/

				touchPos = -1;

			} else {
				/*touchedLeft = false;
				touchedRight = false;*/
				touchPos = 0;
			}
			//check if using this steering method. If so, break.
			if (touchPos != 0) {
				Steer(touchPos);
				return;
			}

			if (Input.GetMouseButton(0))
			{
				//print("moused");
				float mousePos = Input.mousePosition.x;

				if (mousePos > halfScreen + halfScreen / 4)
				{
					touchPos = 1;
					/*touchedRight = true;
					touchedLeft = false;*/
				}
				else if (mousePos < halfScreen - halfScreen / 4)
				{
					touchPos = -1;
					/*touchedLeft = true;
					touchedRight = false;*/
				}
				else
				{
					touchPos = 0;
					/*touchedLeft = false;
					touchedRight = false;*/
				}
				//print(touchPos);

				//rb.AddForce(touchPos * speed /** Time.fixedDeltaTime*/, 0, 0);
			}
			else
			{
				touchPos = 0;
			}

			//check if using this steering method. If so, break.
			if (touchPos != 0) {
				Steer(touchPos);
				return;
			}

			if (Input.touchCount > 0)
         	{
	            float initialTouchPos = Input.GetTouch (0).position.x;
				


				//touchPos = (touchPos - halfScreen) / halfScreen;
				if (initialTouchPos > halfScreen + halfScreen / 5)
				{
					touchPos = 1;
					/*
					touchedRight = true;
					touchedLeft = false;
					*/

				}
				else if (initialTouchPos < halfScreen - halfScreen / 5 )
				{
					touchPos = -1;
					/*
					touchedLeft = true;
					touchedRight = false;
					*/
				} 
				else {
					touchPos = 0;
					/*
					touchedLeft = false;
					touchedRight = false;
					*/
				}
				print("Touch speed = " + touchPos * agility);

				//rb.AddForce(touchPos * (speed / 2f) /* Time.fixedDeltaTime*/, 0, 0);

				//debugCurrentSpeed.text = "CurrentSpeed = " + (touchPos * speed /* Time.fixedDeltaTime*/).ToString();;

//				if (touchPos < Screen.width/2)
//				{
//				 	print(touchPos / halfScreen * -1);
//				}
//				else if (touchPos > Screen.width/2)
//				{
//					print(touchPos / halfScreen - halfScreen);
//				}
        	 } else {
        	 	/*
        	 	touchedLeft = false;
        	 	touchedRight = false;*/
        	 	touchPos = 0;
			}

			//check if using this steering method. If so, break.
			if (touchPos != 0) {
				Steer(touchPos);
				return;
			}


			/*if (touchedLeft) 
			{
        	 	rb.AddForce(-1 * speed, 0, 0);
			}
			else if (touchedRight) 
			{
				rb.AddForce(speed, 0, 0);
			}*/
		}
	}

	void Steer(int touchPos)
	{

			//add speed based on drection
			rb.AddForce(touchPos * agility, 0, 0);
	}

	/*IEnumerator GroundChecker()
	{
		RaycastHit hit;

		while (true)
		{
			if (Physics.Raycast(transform.position, -transform.up, out hit, 3))
			{
				//is our ray seeing the ground?
				if (hit.collider.gameObject.tag == "Ground")
				{
					if (hit.distance > 0.9f)
					{
						onGround = false;
					}
					else
					{
						onGround = true;
					}
				}
			}
			yield return new WaitForSeconds(0.1f);
		}
	}*/

	/*public void AlterDrivingfromWeather (int weather)
	{
		if (weather == 0)
		{
			rb.drag = 1;
			speed -= 500;
		}
		else
		{
			rb.drag = 0.5f;
			speed -= 500;
		}
	}*/

	public void Crash()
	{
		health = 0;
		GameOver();
		healthBar.gameObject.SetActive(false);
	}

	public void AddHealth (int value)
	{
		health += value;
		UpdateUI(-value);	
	}

	IEnumerator InvulnCooldown()
	{
		canBeDamaged = false;
		yield return new WaitForSeconds(invulnTime);
		canBeDamaged = true;
	}

	public void DoDamage (int damage)
	{
		//adjust damage for strength
		//damage *= strength;
		damage = Mathf.RoundToInt(damage * strength);

		health -=  damage;


		if (health > 0)
		{
			UpdateUI(damage);
		}
		else
		{
		/*
			healthText.text = "Health: 0";
			GameOver();

			healthBar.gameObject.SetActive(false);
			*/
			Crash();
		}
		/*
		if (potholeCounter < 3)
		{
			potholeCounter++;
			potholeCounterText.text = "Hit potholes: " + potholeCounter.ToString();
		}
		else
		{
			GameOver();
		}*/
	}

	void UpdateUI (int value)
	{
		healthText.text = "Health: " + health;

		float newWidth = healthBarIncrements * value;
		healthBar.rectTransform.sizeDelta = new Vector2(healthBar.rectTransform.sizeDelta.x - newWidth, healthBar.rectTransform.sizeDelta.y);

		float fHealth = health;
		float colorLerpDelta = fHealth / 100;
		print("health: " + health + " div 100: " + fHealth / 100 + " color: " + colorLerpDelta);
		healthColor = Color.Lerp(redColor, greenColor, colorLerpDelta);
		healthBar.color = healthColor;
	}

	

	private void OnCollisionEnter(Collision collision)
	{
		
		if (collision.gameObject.tag == "NPC")
		{
			//int curDamage = Mathf.RoundToInt(collision.relativeVelocity.magnitude * 1.1f);
			//print("Hit car. Damage is: " + curDamage);
			//changed to static damage value for hitting another car, same as large pothole
			if (canBeDamaged) 
			{
				DoDamage(sideswipeDamage);
				StartCoroutine(InvulnCooldown());
			}

		}
		
		
	}

	void GameOver()
	{
		crashed = true;
		GameManager.instance.GameOver();
	}


}
