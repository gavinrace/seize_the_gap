﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lightpost : MonoBehaviour {

	Renderer model;
	GameObject physicsPole;
	public float speedMultiplier;
	bool hit = false;

	// Use this for initialization
	void Start () {
		model = transform.GetChild(0).GetComponent<Renderer>();
	}

	void OnTriggerEnter (Collider col)
	{
		if (col.gameObject.tag == "Finish" && !hit)
		{
			hit = true;
			model.enabled = false;
			GameManager.instance.player.DoDamage(GetComponent<ObjectDamage>().damage);
			SpawnPhysicsPole();
		}
	}

	void SpawnPhysicsPole()
	{
		physicsPole = TrafficManager.instance.GetComponent<LightpostPooler>().GetPooledLightpole();
		physicsPole.transform.position = transform.position;
		physicsPole.transform.rotation = transform.rotation;
		//get current movement speed
		float moveSpeed = MovementManager.instance.movementSpeed;
		physicsPole.GetComponent<ConstantForce>().force = -transform.forward * moveSpeed;

		//print("spawn physics pole");
	}

	//re-enable the model when it is put back in the pool
	void OnDisable ()
	{
		if (model == null)
		{
			model = transform.GetChild(0).GetComponent<Renderer>();
		}
		model.enabled = true;
		hit = false;
	}
}
