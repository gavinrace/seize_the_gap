﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TargetFrameRater : MonoBehaviour {

	public Button thirtyfps;
	public Button sixtyfps;
	public bool isMenu;

	private void OnEnable()
	{
		StartCoroutine(ChangeFrameRate());
		//DontDestroyOnLoad(gameObject);
	}

	IEnumerator ChangeFrameRate ()
	{
		yield return new WaitForSeconds(0.2f);


		Application.targetFrameRate = FileManager.instance.currentGameData.frameRate;

		if (isMenu)
		{
			StartButtons();
		}
	}

	void StartButtons()
	{
		//print("Current frame rate = " + FileManager.instance.currentGameData.frameRate);
		if (FileManager.instance.currentGameData.frameRate == 30)
		{
			thirtyfps.interactable = false;
			sixtyfps.interactable = true;
		}
		else
		{
			thirtyfps.interactable = true;
			sixtyfps.interactable = false;
		}
	}

	public void Changer(bool is30)
	{
		GameData data = FileManager.instance.currentGameData;

		if (is30)
		{
			thirtyfps.interactable = false;
			sixtyfps.interactable = true;
			data.frameRate = 30;
			FileManager.instance.Save(data);			
		}
		else
		{
			thirtyfps.interactable = true;
			sixtyfps.interactable = false;
			data.frameRate = 60;
			FileManager.instance.Save(data);			
		}

		ChangeFrameRate();

		

		
		
	}
}
