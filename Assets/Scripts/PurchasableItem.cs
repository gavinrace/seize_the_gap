﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PurchasableItem : MonoBehaviour {

	public enum category
	{
		mainColor,
		accentColor,
		topper,
		vehicle,
		utility
	};
	public category itemCategory;
	public int price;
	public int itemIndex;

	/*
	public bool isSelector;

	private void Start()
	{
		if (isSelector)
		{
			Invoke("PrepareVisuals", 0.8f);
		}
	}
	/*
	private void PrepareVisuals()
	{
		if (itemCategory == category.mainColor)
		{
			GetComponent<Image>().color = VehicleModHandler.instance.availableMainColors[itemIndex];
		}
		else if (itemCategory == category.accentColor)
		{
			GetComponent<Image>().color = VehicleModHandler.instance.availableAccentColors[itemIndex];
		}

		transform.GetComponentInChildren<Text>().text = price.ToString() + "SP";
	}*/
}
