﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

	public static GameManager instance;
	public bool inLevel;
	public CarManager player;
	private int score;
	public float distanceTraveled;
	public int earnedCash;
	public Text distanceText;
	public GameObject newBestDistance;

	public int travelTime;
	public List<int> levelThresholds;
	public int currentLevel = 0;
	public Text scoreText;

	[Header("Game Over Menu")]
	public GameObject overlayMenu;
	public Text overlayHeader;
	public Text finalScore;
	public Text finalDistance;
	public GameObject pauseButton;
	public GameObject adButton;
	//public Text totalStylePointsText;


	public bool paused;
	private bool gameOver;


	private bool otherSideBonus;

	[Header("DEBUG LEVEL ACCELERATOR")]
	public int debugLvlAccel;
	public int scoreDistanceRatio;

	// Use this for initialization
	void Start()
	{		
		instance = this;
		if (inLevel)
		{
			StartCoroutine(ScoreForTime());
		}

		earnedCash = 0;
		//ParseLoadedFile();
	}

	//DEBUG ADD TO SCORE DELETE EVENTUALLY
	void Update ()
	{
		if (Input.GetKeyDown(KeyCode.M))
		{
			distanceTraveled += 1;
		}
	}

	public void PauseGame()
	{
		if (!paused)
		{
			Time.timeScale = 0;
			paused = true;

			ToggleMenu(true);
		}
		else
		{
			ToggleMenu(false);

			Time.timeScale = 1;
			paused = false;
		}
	}

	public void ToggleMenu(bool state)
	{
		overlayMenu.SetActive(state);
		print("max saved data  = " + FileManager.instance.currentGameData.maxDistance);
	}

	public void StartOtherSideBonus()
	{
		otherSideBonus = true;
		StartCoroutine(BonusScore());
	}
	public void StoptOtherSideBonus()
	{
		otherSideBonus = false;
	}

	IEnumerator BonusScore()
	{
		while (otherSideBonus && !player.crashed)
		{
			yield return new WaitForSeconds(0.1f);
			//UpdateScore(1);
			
			//UPDATE STAT TRACKER
			StatTracker.instance.ChangeStat(2, 1, 0f);
		}
	}

	IEnumerator ScoreForTime()
	{
		while (!player.crashed)
		{
			yield return new WaitForSeconds(0.2f);
			//UpdateScore(1);
			MovementManager.instance.IncreaseMovementSpeed(0.01f);

			//add to distance traveled. current speed every * .2.2 seconds to get miles traveled in feet
			float distanceIncrement = (MovementManager.instance.movementSpeed * 2) * 0.0005f;
			distanceTraveled += distanceIncrement;
			distanceText.text = distanceTraveled.ToString("F2") + " miles";

			//UPDATE STAT TRACKER
			StatTracker.instance.ChangeStat(0, 0, distanceIncrement);

			//add to plain time traveled
			travelTime++;
			
			//UPDATE STAT TRACKER
			//StatTracker.instance.ChangeStat(1, 1, 0f);

			if (debugLvlAccel > 0)
			{
				travelTime += debugLvlAccel;
			}


			if (travelTime <= levelThresholds[3])
			{
				if (travelTime >= levelThresholds[currentLevel])
				{
					currentLevel++;



					print("Now in level " + currentLevel);
					LevelChange();
				}
			}
		}

	}

	public void UpdateScore(int value)
	{
		score += value;
		scoreText.text = "Score: " + score.ToString();

		//UPDATE STAT TRACKER
		//StatTracker.instance.ChangeStat(0, value, 0);


	}

	public int ReturnScore()
	{
		//return score;
		return Mathf.RoundToInt(scoreDistanceRatio * distanceTraveled);
	}

	void LevelChange()
	{
		//UPDATE STAT TRACKER
		StatTracker.instance.ChangeStat(1, 1, 0);

		switch (currentLevel)
		{
			case 1:
				//add cars
				TrafficManager.instance.PubStartSpawn();
				break;
			case 2:
				//add dividers
				MovementManager.instance.movingObjects.Add(GameObject.Find("Median"));
				break;
			case 3:
				//add rain and more potholes
				GetComponent<PotholeManager>().minDelay -= 0.4f;
				GetComponent<PotholeManager>().maxDelay -= 0.8f;
				WeatherManager.instance.WeatherSwitch();
				break;
			case 4:
				//switch to snow
				WeatherManager.instance.WeatherSwitch();
				//reduce potholes slightly
				GetComponent<PotholeManager>().minDelay += 0.2f;
				GetComponent<PotholeManager>().maxDelay += 0.6f;
				break;
			default:
				break;
		}
	}

	public void GameOver()
	{
		if (!gameOver)
		{
			gameOver = true;

			//work on the rate later
			print("final score = " + scoreDistanceRatio + " * " + distanceTraveled + " = " + scoreDistanceRatio * Mathf.RoundToInt(distanceTraveled));
			earnedCash = scoreDistanceRatio * Mathf.RoundToInt(distanceTraveled);
			UpdateScore(earnedCash);


			TrafficManager.instance.GameOver();
			MovementManager.instance.PubGameOver();
			instance.gameObject.GetComponent<PotholeManager>().gameOver = true;

			//hide small text
			scoreText.gameObject.SetActive(false);
			distanceText.gameObject.SetActive(false);

			//update game over menu

			finalScore.text = "Score: " + score.ToString();
			finalDistance.text = distanceTraveled.ToString("F2") + " miles";

			//check if new distance best
			if (distanceTraveled > FileManager.instance.currentGameData.maxDistance) {
				newBestDistance.SetActive(true);
			}
			overlayHeader.text = "Game Over";
			overlayMenu.SetActive(true);
			adButton.SetActive(true);
			pauseButton.SetActive(false);


			//UPDATE STAT TRACKER
			//StatTracker.instance.ChangeStat(3, score, 0);
			StatTracker.instance.GameCompleteStatCheck();

			//Update total points
			StylePointHandler.instance.GameOver();

			//clean up challenges and save file
			StatTracker.instance.GameOver();




		}
	}

	public void MaxDistanceAchieved()
	{
		newBestDistance.SetActive(true);
	}

	public void ReloadTheScene()
	{
		Scene scene = SceneManager.GetActiveScene();
		SceneManager.LoadScene(scene.name);
	}

}
