﻿using UnityEditor;
using UnityEngine;

public class VertexColorizer : EditorWindow
{
	private Color newMeshColor = Color.cyan;

	[MenuItem("Window/VertexColorizer")]
	public static void DoGenerateVertexColors()
	{
		VertexColorizer window = (VertexColorizer)EditorWindow.GetWindow(typeof(VertexColorizer));
	}

	void OnGUI()
	{
		EditorGUILayout.Space();
		EditorGUILayout.LabelField("Vertex Colorizer");
		EditorGUILayout.Separator();
		newMeshColor = EditorGUILayout.ColorField("New Color", newMeshColor);
		EditorGUILayout.Space();

		if (GUILayout.Button("Colorize!"))
			ChangeColor();
	}

	void ChangeColor()
	{
		Transform[] meshes = Selection.transforms;

		foreach (Transform item in meshes)
		{
			/*
			Mesh mesh;

			MeshFilter mf = item.GetComponent<MeshFilter>();   //a better way of getting the meshfilter using Generics
			Mesh meshCopy = Mesh.Instantiate(mf.sharedMesh) as Mesh;  //make a deep copy
			mesh = mf.mesh = meshCopy;

			Vector3[] vertices = mesh.vertices;*/

			Mesh mesh = item.GetComponent<MeshFilter>().sharedMesh;
			Vector3[] vertices = mesh.vertices;


			// create new colors array where the colors will be created.
			Color[] colors = new Color[vertices.Length];

			for (int i = 0; i < vertices.Length; i++)
				colors[i] = newMeshColor;

			// assign the array of colors to the Mesh.
			mesh.colors = colors;
		}
		
	}
}