﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PauseMenuUpdater : MonoBehaviour {

	public Text curScoreText;
	public Text totalStylePointsText;
	public Text distanceText;
			
	void OnEnable()
	{
		//update stuff when panel is turned on
		curScoreText.text = "Score: " + GameManager.instance.ReturnScore();
		totalStylePointsText.text = "Total:  §" +  StylePointHandler.instance.readableStylePoints;
		distanceText.text = GameManager.instance.distanceTraveled.ToString("F2") + " miles";

	}
}
