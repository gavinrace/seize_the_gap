// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.34 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.34;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:9361,x:33209,y:32712,varname:node_9361,prsc:2|normal-7545-OUT,emission-2460-OUT,custl-5085-OUT;n:type:ShaderForge.SFN_LightAttenuation,id:8068,x:32734,y:33086,varname:node_8068,prsc:2;n:type:ShaderForge.SFN_LightColor,id:3406,x:32734,y:32952,varname:node_3406,prsc:2;n:type:ShaderForge.SFN_LightVector,id:6869,x:31858,y:32654,varname:node_6869,prsc:2;n:type:ShaderForge.SFN_NormalVector,id:9684,x:31858,y:32782,prsc:2,pt:True;n:type:ShaderForge.SFN_HalfVector,id:9471,x:31858,y:32933,varname:node_9471,prsc:2;n:type:ShaderForge.SFN_Dot,id:7782,x:32070,y:32697,cmnt:Lambert,varname:node_7782,prsc:2,dt:1|A-6869-OUT,B-9684-OUT;n:type:ShaderForge.SFN_Dot,id:3269,x:32070,y:32871,cmnt:Blinn-Phong,varname:node_3269,prsc:2,dt:1|A-9684-OUT,B-9471-OUT;n:type:ShaderForge.SFN_Multiply,id:2746,x:32465,y:32895,cmnt:Specular Contribution,varname:node_2746,prsc:2|A-7782-OUT,B-3269-OUT,C-4865-RGB;n:type:ShaderForge.SFN_Multiply,id:1941,x:32465,y:32693,cmnt:Diffuse Contribution,varname:node_1941,prsc:2|A-5927-RGB,B-7782-OUT;n:type:ShaderForge.SFN_Color,id:5927,x:32070,y:32534,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_5927,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.7720588,c2:0.1589533,c3:0.1589533,c4:1;n:type:ShaderForge.SFN_Add,id:2159,x:32734,y:32812,cmnt:Combine,varname:node_2159,prsc:2|A-1941-OUT,B-2746-OUT;n:type:ShaderForge.SFN_Multiply,id:5085,x:32979,y:32952,cmnt:Attenuate and Color,varname:node_5085,prsc:2|A-2159-OUT,B-3406-RGB,C-8068-OUT;n:type:ShaderForge.SFN_Color,id:4865,x:32254,y:33115,ptovrint:False,ptlb:Spec Color,ptin:_SpecColor,varname:node_4865,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.7686275,c2:0.1568628,c3:0.1568628,c4:1;n:type:ShaderForge.SFN_AmbientLight,id:7528,x:32734,y:32646,varname:node_7528,prsc:2;n:type:ShaderForge.SFN_Multiply,id:2460,x:33006,y:32607,cmnt:Ambient Light,varname:node_2460,prsc:2|A-7709-OUT,B-7528-RGB;n:type:ShaderForge.SFN_NormalVector,id:8338,x:31999,y:31715,prsc:2,pt:True;n:type:ShaderForge.SFN_ComponentMask,id:5381,x:32188,y:31733,varname:node_5381,prsc:2,cc1:0,cc2:1,cc3:2,cc4:-1|IN-8338-OUT;n:type:ShaderForge.SFN_Append,id:4626,x:32878,y:31776,varname:node_4626,prsc:2|A-5381-R,B-2575-OUT,C-5381-B;n:type:ShaderForge.SFN_Multiply,id:6255,x:33061,y:31776,varname:node_6255,prsc:2|A-4626-OUT,B-4626-OUT;n:type:ShaderForge.SFN_FragmentPosition,id:8083,x:32000,y:31989,varname:node_8083,prsc:2;n:type:ShaderForge.SFN_Normalize,id:3133,x:32188,y:31931,varname:node_3133,prsc:2|IN-8083-Y;n:type:ShaderForge.SFN_Append,id:5306,x:32188,y:32072,varname:node_5306,prsc:2|A-8083-Z,B-8083-X;n:type:ShaderForge.SFN_Dot,id:5908,x:32457,y:32054,varname:node_5908,prsc:2,dt:2|A-5381-G,B-3133-OUT;n:type:ShaderForge.SFN_If,id:2575,x:32680,y:31908,varname:node_2575,prsc:2|A-3133-OUT,B-1177-OUT,GT-541-OUT,EQ-5908-OUT,LT-5908-OUT;n:type:ShaderForge.SFN_Vector1,id:1177,x:32457,y:31995,varname:node_1177,prsc:2,v1:0;n:type:ShaderForge.SFN_Dot,id:541,x:32457,y:31842,varname:node_541,prsc:2,dt:1|A-5381-G,B-3133-OUT;n:type:ShaderForge.SFN_ChannelBlend,id:8784,x:33271,y:32126,varname:node_8784,prsc:2,chbt:0|M-6255-OUT,R-2551-OUT,G-8923-OUT,B-2551-OUT;n:type:ShaderForge.SFN_Vector3,id:2551,x:32983,y:32138,varname:node_2551,prsc:2,v1:0,v2:0,v3:0;n:type:ShaderForge.SFN_Color,id:4079,x:32324,y:32345,ptovrint:False,ptlb:Snow Color,ptin:_SnowColor,varname:node_4079,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.9632353,c2:0.9632353,c3:0.9632353,c4:1;n:type:ShaderForge.SFN_Multiply,id:8923,x:32872,y:32233,varname:node_8923,prsc:2|A-4079-RGB,B-6931-OUT,C-6367-OUT;n:type:ShaderForge.SFN_Slider,id:6931,x:32324,y:32238,ptovrint:False,ptlb:Snow Amount,ptin:_SnowAmount,varname:node_6931,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.85,max:0.85;n:type:ShaderForge.SFN_Add,id:7709,x:33130,y:32415,varname:node_7709,prsc:2|A-5927-RGB,B-8784-OUT;n:type:ShaderForge.SFN_Vector1,id:6367,x:32767,y:32402,varname:node_6367,prsc:2,v1:2;n:type:ShaderForge.SFN_ComponentMask,id:6067,x:33569,y:32133,varname:node_6067,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-8784-OUT;n:type:ShaderForge.SFN_Step,id:3652,x:33796,y:32092,varname:node_3652,prsc:2|A-4825-OUT,B-6067-OUT;n:type:ShaderForge.SFN_Vector1,id:4825,x:33569,y:32052,varname:node_4825,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Tex2d,id:1099,x:33545,y:32434,ptovrint:False,ptlb:node_1099,ptin:_node_1099,varname:node_1099,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:2a2eb704d4d9e8f4b807ea360c9bc16d,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Tex2d,id:7431,x:33587,y:32731,ptovrint:False,ptlb:node_7431,ptin:_node_7431,varname:node_7431,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:bbab0a6f7bae9cf42bf057d8ee2755f6,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Lerp,id:7545,x:33918,y:32589,varname:node_7545,prsc:2|A-1099-RGB,B-7431-RGB,T-2551-OUT;n:type:ShaderForge.SFN_Tex2d,id:6454,x:33737,y:32279,ptovrint:False,ptlb:node_6454,ptin:_node_6454,varname:node_6454,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:6a985dfcd1d3ca94e9753748ed65c697,ntxv:0,isnm:False;proporder:5927-4865-4079-6931-1099-7431-6454;pass:END;sub:END;*/

Shader "Shader Forge/NewShaderTest" {
    Properties {
        _Color ("Color", Color) = (0.7720588,0.1589533,0.1589533,1)
        _SpecColor ("Spec Color", Color) = (0.7686275,0.1568628,0.1568628,1)
        _SnowColor ("Snow Color", Color) = (0.9632353,0.9632353,0.9632353,1)
        _SnowAmount ("Snow Amount", Range(0, 0.85)) = 0.85
        _node_1099 ("node_1099", 2D) = "bump" {}
        _node_7431 ("node_7431", 2D) = "bump" {}
        _node_6454 ("node_6454", 2D) = "white" {}
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles metal 
            #pragma target 3.0
            uniform float4 _Color;
            uniform float4 _SnowColor;
            uniform float _SnowAmount;
            uniform sampler2D _node_1099; uniform float4 _node_1099_ST;
            uniform sampler2D _node_7431; uniform float4 _node_7431_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
                UNITY_FOG_COORDS(7)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos(v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _node_1099_var = UnpackNormal(tex2D(_node_1099,TRANSFORM_TEX(i.uv0, _node_1099)));
                float3 _node_7431_var = UnpackNormal(tex2D(_node_7431,TRANSFORM_TEX(i.uv0, _node_7431)));
                float3 node_2551 = float3(0,0,0);
                float3 normalLocal = lerp(_node_1099_var.rgb,_node_7431_var.rgb,node_2551);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
////// Emissive:
                float3 node_5381 = normalDirection.rgb;
                float node_3133 = normalize(i.posWorld.g);
                float node_2575_if_leA = step(node_3133,0.0);
                float node_2575_if_leB = step(0.0,node_3133);
                float node_5908 = min(0,dot(node_5381.g,node_3133));
                float3 node_4626 = float3(node_5381.r,lerp((node_2575_if_leA*node_5908)+(node_2575_if_leB*max(0,dot(node_5381.g,node_3133))),node_5908,node_2575_if_leA*node_2575_if_leB),node_5381.b);
                float3 node_6255 = (node_4626*node_4626);
                float3 node_8923 = (_SnowColor.rgb*_SnowAmount*2.0);
                float3 node_8784 = (node_6255.r*node_2551 + node_6255.g*node_8923 + node_6255.b*node_2551);
                float3 emissive = ((_Color.rgb+node_8784)*UNITY_LIGHTMODEL_AMBIENT.rgb);
                float node_7782 = max(0,dot(lightDirection,normalDirection)); // Lambert
                float3 finalColor = emissive + (((_Color.rgb*node_7782)+(node_7782*max(0,dot(normalDirection,halfDirection))*_SpecColor.rgb))*_LightColor0.rgb*attenuation);
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles metal 
            #pragma target 3.0
            uniform float4 _Color;
            uniform float4 _SnowColor;
            uniform float _SnowAmount;
            uniform sampler2D _node_1099; uniform float4 _node_1099_ST;
            uniform sampler2D _node_7431; uniform float4 _node_7431_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
                UNITY_FOG_COORDS(7)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos(v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _node_1099_var = UnpackNormal(tex2D(_node_1099,TRANSFORM_TEX(i.uv0, _node_1099)));
                float3 _node_7431_var = UnpackNormal(tex2D(_node_7431,TRANSFORM_TEX(i.uv0, _node_7431)));
                float3 node_2551 = float3(0,0,0);
                float3 normalLocal = lerp(_node_1099_var.rgb,_node_7431_var.rgb,node_2551);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float node_7782 = max(0,dot(lightDirection,normalDirection)); // Lambert
                float3 finalColor = (((_Color.rgb*node_7782)+(node_7782*max(0,dot(normalDirection,halfDirection))*_SpecColor.rgb))*_LightColor0.rgb*attenuation);
                fixed4 finalRGBA = fixed4(finalColor * 1,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
