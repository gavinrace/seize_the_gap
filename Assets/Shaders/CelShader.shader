// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:False,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.4313726,fgcg:0.5019608,fgcb:0.5450981,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:True,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:9361,x:33209,y:32712,varname:node_9361,prsc:2|emission-2460-OUT,custl-5085-OUT,olwid-2544-OUT;n:type:ShaderForge.SFN_LightAttenuation,id:8068,x:32734,y:33086,varname:node_8068,prsc:2;n:type:ShaderForge.SFN_LightColor,id:3406,x:32734,y:32952,varname:node_3406,prsc:2;n:type:ShaderForge.SFN_Multiply,id:1941,x:32466,y:32695,cmnt:Diffuse Contribution,varname:node_1941,prsc:2|A-4834-OUT,B-2977-OUT;n:type:ShaderForge.SFN_Slider,id:5328,x:31448,y:33165,ptovrint:False,ptlb:Gloss,ptin:_Gloss,varname:node_5328,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:0.6;n:type:ShaderForge.SFN_Multiply,id:5085,x:32979,y:32952,cmnt:Attenuate and Color,varname:node_5085,prsc:2|A-1941-OUT,B-3406-RGB,C-8068-OUT;n:type:ShaderForge.SFN_AmbientLight,id:7528,x:32698,y:32587,varname:node_7528,prsc:2;n:type:ShaderForge.SFN_Multiply,id:2460,x:32945,y:32540,cmnt:Ambient Light,varname:node_2460,prsc:2|A-4834-OUT,B-7528-RGB;n:type:ShaderForge.SFN_LightVector,id:5287,x:30797,y:32627,cmnt:Cel shading,varname:node_5287,prsc:2;n:type:ShaderForge.SFN_NormalVector,id:240,x:30797,y:32794,prsc:2,pt:True;n:type:ShaderForge.SFN_Dot,id:4545,x:30983,y:32699,varname:node_4545,prsc:2,dt:1|A-5287-OUT,B-240-OUT;n:type:ShaderForge.SFN_Power,id:1480,x:31247,y:32713,varname:node_1480,prsc:2|VAL-4545-OUT,EXP-5783-OUT;n:type:ShaderForge.SFN_Power,id:8468,x:31433,y:32713,varname:node_8468,prsc:2|VAL-1480-OUT,EXP-1399-OUT;n:type:ShaderForge.SFN_ValueProperty,id:5783,x:31075,y:32877,ptovrint:False,ptlb:GradientSize,ptin:_GradientSize,varname:node_5783,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.76;n:type:ShaderForge.SFN_ValueProperty,id:1399,x:31274,y:32906,ptovrint:False,ptlb:GradientSharpness,ptin:_GradientSharpness,varname:node_1399,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:3.5;n:type:ShaderForge.SFN_OneMinus,id:4885,x:31624,y:32713,varname:node_4885,prsc:2|IN-8468-OUT;n:type:ShaderForge.SFN_Power,id:2192,x:31832,y:32713,varname:node_2192,prsc:2|VAL-4885-OUT,EXP-6253-OUT;n:type:ShaderForge.SFN_Exp,id:6253,x:31513,y:32884,varname:node_6253,prsc:2,et:0|IN-1399-OUT;n:type:ShaderForge.SFN_OneMinus,id:5140,x:32032,y:32713,varname:node_5140,prsc:2|IN-2192-OUT;n:type:ShaderForge.SFN_Clamp01,id:2977,x:32212,y:32713,varname:node_2977,prsc:2|IN-5140-OUT;n:type:ShaderForge.SFN_ComponentMask,id:7271,x:31541,y:32070,varname:node_7271,prsc:2,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-240-OUT;n:type:ShaderForge.SFN_Clamp01,id:4302,x:31722,y:32070,varname:node_4302,prsc:2|IN-7271-G;n:type:ShaderForge.SFN_Lerp,id:4834,x:32179,y:32427,varname:node_4834,prsc:2|A-1055-OUT,B-4947-OUT,T-1418-OUT;n:type:ShaderForge.SFN_Step,id:9403,x:32048,y:32046,varname:node_9403,prsc:2|A-4302-OUT,B-1845-OUT;n:type:ShaderForge.SFN_Vector1,id:1845,x:31899,y:32119,varname:node_1845,prsc:2,v1:0.4;n:type:ShaderForge.SFN_Slider,id:1186,x:32090,y:32250,ptovrint:False,ptlb:Snow Amount,ptin:_SnowAmount,varname:node_7166,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:0.55;n:type:ShaderForge.SFN_Vector3,id:924,x:32247,y:32097,varname:node_924,prsc:2,v1:1,v2:1,v3:1;n:type:ShaderForge.SFN_Lerp,id:1418,x:32438,y:32204,varname:node_1418,prsc:2|A-924-OUT,B-9403-OUT,T-1186-OUT;n:type:ShaderForge.SFN_Cubemap,id:7002,x:31845,y:33115,ptovrint:False,ptlb:Cubemap,ptin:_Cubemap,varname:node_7002,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,cube:28232cc4f9642423a80c21bcc071b296,pvfc:0;n:type:ShaderForge.SFN_Multiply,id:4947,x:31903,y:32549,cmnt:Cubemap blender,varname:node_4947,prsc:2|A-575-RGB,B-2434-OUT;n:type:ShaderForge.SFN_Lerp,id:2434,x:32165,y:33079,varname:node_2434,prsc:2|A-575-RGB,B-7002-RGB,T-5328-OUT;n:type:ShaderForge.SFN_VertexColor,id:575,x:31344,y:32545,varname:node_575,prsc:2;n:type:ShaderForge.SFN_Vector3,id:1055,x:31903,y:32329,varname:node_1055,prsc:2,v1:1,v2:1,v3:1;n:type:ShaderForge.SFN_ValueProperty,id:2544,x:32938,y:33145,ptovrint:False,ptlb:Outline,ptin:_Outline,varname:node_2544,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;proporder:5328-5783-1399-1186-7002-2544;pass:END;sub:END;*/

Shader "Shader Forge/CelShader" {
    Properties {
        _Gloss ("Gloss", Range(0, 0.6)) = 0
        _GradientSize ("GradientSize", Float ) = 0.76
        _GradientSharpness ("GradientSharpness", Float ) = 3.5
        _SnowAmount ("Snow Amount", Range(0, 0.55)) = 0
        _Cubemap ("Cubemap", Cube) = "_Skybox" {}
        _Outline ("Outline", Float ) = 0
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "Outline"
            Tags {
            }
            Cull Front
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 
            #pragma target 2.0
            uniform float _Outline;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                UNITY_FOG_COORDS(0)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.pos = UnityObjectToClipPos( float4(v.vertex.xyz + v.normal*_Outline,1) );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                return fixed4(float3(0,0,0),0);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 
            #pragma target 2.0
            uniform float _Gloss;
            uniform float _GradientSize;
            uniform float _GradientSharpness;
            uniform float _SnowAmount;
            uniform samplerCUBE _Cubemap;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                float4 vertexColor : COLOR;
                LIGHTING_COORDS(2,3)
                UNITY_FOG_COORDS(4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
////// Emissive:
                float node_9403 = step(saturate(normalDirection.rg.g),0.4);
                float3 node_4834 = lerp(float3(1,1,1),(i.vertexColor.rgb*lerp(i.vertexColor.rgb,texCUBE(_Cubemap,viewReflectDirection).rgb,_Gloss)),lerp(float3(1,1,1),float3(node_9403,node_9403,node_9403),_SnowAmount));
                float3 emissive = (node_4834*UNITY_LIGHTMODEL_AMBIENT.rgb);
                float3 finalColor = emissive + ((node_4834*saturate((1.0 - pow((1.0 - pow(pow(max(0,dot(lightDirection,normalDirection)),_GradientSize),_GradientSharpness)),exp(_GradientSharpness)))))*_LightColor0.rgb*attenuation);
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
