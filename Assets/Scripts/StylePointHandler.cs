﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StylePointHandler : MonoBehaviour {

	public static StylePointHandler instance;
	private int totalStylePoints;

	public int readableStylePoints
	{
		get
		{
			return totalStylePoints;
		}
	}

	public Text SPTotal;


	void Start()
	{
		instance = this;
	}

	public void SyncLoadedStylePoints()
	{
		print("attempting to sync SP");
		totalStylePoints = FileManager.instance.ReturnLoadedStylePoints();
		print("$ = " + totalStylePoints);
		UpdateUI();
	}
	
	public void ParseLoadedFile(int loadedStylePoints)
	{
		totalStylePoints = loadedStylePoints;
		UpdateUI();
	}

	public void GameOver()
	{
		//when game ends, add points earned in this game to total
		totalStylePoints += GameManager.instance.ReturnScore();

		UpdateUI();
	}

	public void UpdateUI()
	{
		print("Updated SP total ");
		SPTotal.text = "Total:  §" + totalStylePoints;
	}

	public int SpendPoints(int amount)
	{
		int origPoints = totalStylePoints;

		//don't subract negative amounts... because that would be adding... which would be cheating...
		if (amount > 0)
		{
			//now that we are sure it's a positive amount, also make sure there are enough to spend
			if (totalStylePoints - amount >= 0)
			{
				totalStylePoints -= amount;
				UpdateUI();

				FileManager.instance.currentGameData.stylePoints = totalStylePoints;
				FileManager.instance.Save(FileManager.instance.currentGameData);
			}
			else
			{
				//player does not have enough to spend
				return 0;
			}
		}

		if (origPoints - totalStylePoints == amount)
		{
			//points were properly subtracted
			UpdateUI();

			return 1;
		}
		else
		{
			//points were not properly subtracted
			return 3;
		}


	}
}
