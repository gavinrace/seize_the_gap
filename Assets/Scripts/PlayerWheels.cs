﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWheels : MonoBehaviour {

	private TiresManager tires;
	private CarManager player;
	public bool side;
	// Use this for initialization
	void Start () {
		tires = transform.parent.GetComponent<TiresManager>();
		player = transform.parent.GetComponent<CarManager>();
	}

	void OnTriggerEnter (Collider col)
	{
		if (col.gameObject.tag == "Pothole" || col.gameObject.tag == "Lightpole")
		{
			if (col.gameObject.tag == "Pothole")
			{
				//run this through the tire manager to see if we have any protection on them
				tires.HitPothole(col.gameObject.GetComponent<ObjectDamage>().damage, side);
				StatTracker.instance.ChangeStat(4,1,0);
			}
			else
			{
				//immediately damage the player for hitting a lightpole
				player.DoDamage(col.gameObject.GetComponent<ObjectDamage>().damage);
				transform.parent.GetComponent<Animator>().SetTrigger("HitObject");
				StatTracker.instance.ChangeStat(5,1,0);
			}
			
			

		}
	}

}
