﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjReuser : MonoBehaviour {

	public bool deactivate;
	public bool reset;
	public bool reactivateChildrenOnDeactivation;
	public float resetPosition;

	public Transform baseObj;

	//public Renderer weatherable;
	public int materialIndx;

	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "Resetter")
		{
			if (deactivate)
			{
				if (baseObj != null)
				{
					baseObj.gameObject.SetActive(false);
				} 
				else {
					gameObject.SetActive(false);
				}

				if (reactivateChildrenOnDeactivation)
				{
					foreach (Transform child in baseObj)
					{
						if (child.gameObject.activeInHierarchy == false)
						{
							child.gameObject.SetActive(true);
						}
					}
				}
			}
			else if (reset)
			{
				if (baseObj != null)
				{
					baseObj.position = new Vector3( baseObj.transform.position.x, baseObj.transform.position.y, resetPosition);
				} 
				else {
					transform.position = new Vector3( baseObj.transform.position.x, baseObj.transform.position.y, resetPosition);
				}
			}
			
		}
	}
}
