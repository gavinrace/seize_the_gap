﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SparkTrigger : MonoBehaviour {

	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "NPC" || col.gameObject.tag == "Lightpole")
		{
			//print("SPARKS: " + col.gameObject);
			GetComponent<ParticleSystem>().Play();
		}
		
	}
}
