﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrafficManager : MonoBehaviour {

	[Header("Common Cars")]
	public List<GameObject> commonCars;
	public List<GameObject> commonCarPool;
	[Header("Rare Cars")]
	public List<GameObject> rareCars;
	public List<GameObject> rareCarPool;
	[Header("Traffic Stuff")]
	public List<Transform> lanes;
	public Transform spawnPos;
	public float minDelay;
	public float maxDelay;

	public bool notMenu;

	//private float speed;
	private bool gameOver;

	public static TrafficManager instance;

	// Use this for initialization
	void Start () {
		instance = this;
		//StartCoroutine(SpawnTraffic());
	}
	
	// Update is called once per frame
	void Update () {

	}

	IEnumerator GetGlobalSpeed()
	{
		yield return new WaitForSeconds(0.2f);
		//speed = MovementManager.instance.movementSpeed;
	}

	public void PubStartSpawn()
	{
		StartCoroutine(SpawnTraffic());
	}

	IEnumerator SpawnTraffic ()
	{
		bool sameSide;
		float rarityDeterminer;

		while(!gameOver)
		{
			float delay = Random.Range(minDelay,maxDelay);
			yield return new WaitForSeconds(delay);

			rarityDeterminer = Random.Range(0, 100);
			GameObject car;
			if (rarityDeterminer < 85)
			{
				car = GetPooledCommonCar();
			}
			else
			{
				car = GetRareCar();
			}

			//reset rotaion befor deciding side of the street
			car.transform.eulerAngles = Vector3.zero;

			sameSide = car.GetComponent<TrafficParams>().ChooseSide();

			int laneNum = Random.Range(0, 100);
			if (sameSide)
			{
				if (laneNum > 50)
				{
					laneNum = 3;
				}
				else {
					laneNum = 2;
				}

				//alteration for menu version of traffic
				if (notMenu)
				{
					car.transform.position = new Vector3(lanes[laneNum].position.x, spawnPos.position.y, spawnPos.position.z);
				}
				else
				{
					car.transform.position = new Vector3(lanes[laneNum].position.x, spawnPos.position.y, lanes[laneNum].position.z);
					//set the "same side" prop to false even though it's true so that it chooses speeds for opposite side traffic
					car.GetComponent<TrafficParams>().sameSide = false;
				}
			}
			else{
				if (laneNum > 50)
				{
					laneNum = 1;
				}
				else {
					laneNum = 0;
				}
				car.transform.eulerAngles = new Vector3(0, 180, 0);
				car.transform.position = new Vector3(lanes[laneNum].position.x, spawnPos.position.y, spawnPos.position.z);
			}

			car.GetComponent<TrafficParams>().laneNum = laneNum;
			car.GetComponent<TrafficParams>().lanePos = lanes[laneNum].position;
			car.GetComponent<TrafficParams>().ChooseSpeed();

			//after setting all paraemters, activate
			car.SetActive(true);
		}
	}

	GameObject GetPooledCommonCar ()
	{
		for (int i = 0; i < commonCarPool.Count; i++)
			{
				if (commonCarPool[i].activeInHierarchy == false)
				{
					//print("found inactive car to use");
					//carPool[i].SetActive(true);
					return commonCarPool[i];
				}
				else {
					//print("car is active");
				}
			}
		//print("went through whole list and found no inactive cars");
		//none left
			GameObject newCommonCar/* = new GameObject()*/;
		
		newCommonCar = GameObject.Instantiate(commonCars[Random.Range(0, commonCars.Count)]);
		newCommonCar.SetActive(false);
		commonCarPool.Add(newCommonCar);
			return newCommonCar;
	}

	GameObject GetRareCar()
	{
		for (int i = 0; i < commonCarPool.Count; i++)
		{
			if (commonCarPool[i].activeInHierarchy == false)
			{
				//print("found inactive car to use");
				//carPool[i].SetActive(true);
				return commonCarPool[i];
			}
			else
			{
				//print("car is active");
			}
		}

		//Get new rare car
		GameObject newCar/* = new GameObject()*/;
		
		newCar = GameObject.Instantiate(rareCars[Random.Range(0, rareCars.Count)]);
		newCar.SetActive(false);
		rareCarPool.Add(newCar);
		return newCar;
	}

	public void GameOver ()
	{
		gameOver = true;

		StartCoroutine(TrafficGameOverChange());

	}

	IEnumerator TrafficGameOverChange ()
	{
		//delay so that hit car can change states first

		yield return new WaitForSeconds(0.1f);

		foreach (GameObject car in commonCarPool)
		{
			if (car.activeInHierarchy)
			{
				car.GetComponent<TrafficAi>().GameOver();
			}
		}

		foreach (GameObject rareCar in rareCarPool)
		{
			if (rareCar.activeInHierarchy)
			{
				rareCar.GetComponent<TrafficAi>().GameOver();
			}
		}
	}


}
