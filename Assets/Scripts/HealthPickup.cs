﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickup : MonoBehaviour {

	public int value;

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Finish")
		{
			if (other.transform.parent.parent.parent.GetComponent<CarManager>().health < 100)
			{
				if (other.transform.parent.parent.parent.GetComponent<CarManager>().health + value > 100)
				{
					value = 100 - other.transform.parent.parent.parent.GetComponent<CarManager>().health;
				}

				//do negative damage to replenish some health
				other.transform.parent.parent.parent.GetComponent<CarManager>().AddHealth(value);

				gameObject.SetActive(false);
			}
			

			
		}
	}
}
