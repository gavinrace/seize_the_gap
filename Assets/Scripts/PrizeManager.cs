﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PrizeManager : MonoBehaviour {

	//make list of prize text and images
	public List<string> prizeNames;
	public List<string> prizeDescriptions;

	public Animator panelAnimator;
	public Text prizeName;
	public Text prizeDesc;


	public void GetNextPrize()
	{
		int curPrizeval = FileManager.instance.currentGameData.currentPrize;
		GameData curData = FileManager.instance.currentGameData;

		/*switch (curPrizeval)
		{
			case 0:
				//if this is the first prize, it will enable the crate by just having current prize above 0
				break;
			default:
				break;
		}*/

		AnnouncePrize(curPrizeval);

		//increase prize val and save it
		curPrizeval++;
		curData.currentPrize = curPrizeval;

		FileManager.instance.Save(curData);
	}

	void AnnouncePrize (int prizeNum)
	{
		//show panel announcing what new price is
		prizeName.text = prizeNames[prizeNum];
		prizeDesc.text = prizeDescriptions[prizeNum];

		panelAnimator.SetTrigger("Show");
	}


}
