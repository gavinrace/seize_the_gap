﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementManager : MonoBehaviour {

	public static MovementManager instance;
	public List<GameObject> movingObjects;
	public float movementSpeed;
	public float maxMovementSpeed;

	// Use this for initialization
	void Start () {
		instance = this;
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		foreach (GameObject obj in movingObjects)
		{
			if (obj.activeInHierarchy)
			{
				obj.transform.Translate(-obj.transform.forward * Time.deltaTime * movementSpeed);
			}	
		}
		
	}

	public void IncreaseMovementSpeed(float increase)
	{
		if (movementSpeed < maxMovementSpeed)
		{
			movementSpeed += increase;
		}
	}

	public void PubGameOver()
	{
		StartCoroutine(GameOver());
	}

	IEnumerator GameOver ()
	{
		float difference = movementSpeed / 10;

		while (movementSpeed > 0)
		{
			movementSpeed -= difference;
			if (movementSpeed < 0)
			{
				movementSpeed = 0;
			}
			yield return new WaitForSeconds(0.1f);
		}
	}

}
