﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//[ExecuteInEditMode]
public class VertexColorChooser : MonoBehaviour {

	public bool useRandomColor;
	public enum ObjType
	{
		buildingMain,
		buildingTrim,
		vehicleMain,
		vehicleAccent
	};

	public ObjType objectType;

	public Color startingColor;
	public float randNum = 0;
	public 	int chosenNum = 0;
	
	
	// Use this for initialization
	void OnEnable () {
		if (useRandomColor)
		{
			StartCoroutine(SpawnColorDelay());
			//ChangeColor(startingColor);
		}
		else
		{
			ChangeColor(startingColor);
		}
	}

	IEnumerator SpawnColorDelay ()
	{
		yield return new WaitForSeconds(0.1f);
		ChooseRandomColor();
	}


	void ChooseRandomColor()
	{
		Color randColor = Color.magenta;


		switch (objectType) {

			case ObjType.buildingMain:
			randNum = Random.Range(0, (RandomColorLibrary.instance.buildingMain.Count -1) * 10);
			randNum = randNum / 10;
			chosenNum = Mathf.RoundToInt(randNum);
			randColor = RandomColorLibrary.instance.buildingMain[chosenNum];

			break;

			case ObjType.buildingTrim:
			randNum = Random.Range(0, (RandomColorLibrary.instance.buildingTrim.Count - 1) * 10);
			randNum = randNum / 10;
			chosenNum = Mathf.RoundToInt(randNum);
			randColor = RandomColorLibrary.instance.buildingTrim[chosenNum];
			break;

			case ObjType.vehicleMain:
			randNum = Random.Range(0, (RandomColorLibrary.instance.vehicleMain.Count - 1) * 10);
			randNum = randNum / 10;
			chosenNum = Mathf.RoundToInt(randNum);
			randColor = RandomColorLibrary.instance.vehicleMain[chosenNum];
			break;

			case ObjType.vehicleAccent:
			randNum = Random.Range(0, (RandomColorLibrary.instance.vehicleAccent.Count - 1) * 10);
			randNum = randNum / 10;
			chosenNum = Mathf.RoundToInt(randNum);
			randColor = RandomColorLibrary.instance.vehicleAccent[chosenNum];
			break;

		default:
			break;
		}

		ChangeColor(randColor);
	}

	public void ChangeColor(Color theColor)
	{
		Mesh mesh = GetComponent<MeshFilter>().mesh;
		Vector3[] vertices = mesh.vertices;


		// create new colors array where the colors will be created.
		Color[] colors = new Color[vertices.Length];

		for (int i = 0; i < vertices.Length; i++)
			colors[i] = theColor;

		// assign the array of colors to the Mesh.
		mesh.colors = colors;
	}
}
