﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrafficAi : MonoBehaviour {

	Rigidbody rb;
	TrafficParams carParams;
	Vector3 lanePos;
	float speed;
	//float globalSpeed;
	//how far from center of lane?
	public float laneDiff;
	//if on same side, = -1 because objec tis technically moving in reverse
	int sameSide = 0;
	//public float newLaneDelta;
	//float newLanePos;

	//add delay on ability to crash so they don't crash on spawn
	bool canCrash;

	//add time to wait before changing lanes so they don't continue to go back and forth
	public float laneChangeDelay;
	bool canChangeLanes = true;

	public Transform sensor;
	public float scanRate;
	
	public enum state
	{
		init,
		driving,
		changingLanes,
		crashed,
		gameOver
	};

	public state carState;

	// Use this for initialization
	void Awake () {
		rb = GetComponent<Rigidbody>();
		carParams = GetComponent<TrafficParams>();
	}

	private void OnEnable()
	{
		//we need to wait a tiny bit for init
		carState = state.init;
		StartCoroutine(FindParameterDelay());
		canCrash = false;
		StartCoroutine(CanCrashDelay());
	}

	IEnumerator FindParameterDelay ()
	{
		yield return new WaitForSeconds(0.1f);


		speed = carParams.speed;

		if (carParams.sameSide)
		{
			sameSide = -1;
			//speed = carParams.speed * globalSpeed;
		}
		else
		{
			sameSide = 1;
			//speed = carParams.speed / globalSpeed;
		}

		lanePos = carParams.FindLanePos();

		
		carState = state.driving;
		StartCoroutine(ScanForObstacles());
	}

	// Update is called once per frame
	void FixedUpdate () {

		switch (carState)
		{
			case state.init:
				break;

			case state.driving:
				MoveCar();
				CheckLanePosition();
				break;

			case state.changingLanes:
				MoveCar();
				CheckLanePosition();
				break;

			case state.crashed:
				//it's not driving anymore, but when it's crashed it still has to move with the scenery
				MoveCar();
				break;
			case state.gameOver:
				MoveCar();
				break;

			default:
				break;
		}
	}

	IEnumerator CanCrashDelay ()
	{
		yield return new WaitForSeconds(2);
		canCrash = true;
	}

	void MoveCar()
	{
		rb.AddForce(sameSide * transform.forward * 180 * speed * Time.deltaTime);
	}

	void CheckLanePosition()
	{
		//find how far away from center of lane the car is
		laneDiff = Mathf.Abs(transform.position.x - lanePos.x);

		if (laneDiff > 0.1f)
		{
			//GetComponent<Renderer>().material.color = Color.white;
			CorrectDriving();
		}
		//it is close enough to center of desired to stop being considered changing lanes
		else if (carState == state.changingLanes)
		{
			//catchall to prevent car resurrection
			if (carState != state.crashed)
			{
				//start delay so that he doesn't immediately go back into changing lanes
				StartCoroutine(LanechangeDelay());

				carState = state.driving;
				StartCoroutine(ScanForObstacles());
			}
		}
	}

	IEnumerator LanechangeDelay ()
	{
		canChangeLanes = false;
		yield return new WaitForSeconds(laneChangeDelay);
		canChangeLanes = true;
	}

	void CorrectDriving()
	{
		laneDiff = Mathf.Abs(transform.position.x - lanePos.x);

		rb.AddForce((lanePos - transform.position).normalized * 500  * laneDiff * Time.smoothDeltaTime);
		
	}

	IEnumerator ScanForObstacles ()
	{
		Vector3 fwd = sensor.transform.forward;
		RaycastHit hit;

		while (carState == state.driving)
		{
			//Ray for detectin gother cars
			Debug.DrawRay(sensor.position, fwd * 20, Color.green, 0.5f);

			if (Physics.Raycast(sensor.position, fwd, out hit, 20))
			{
				//is our ray seeing another car?
				if (hit.collider.gameObject.tag == "NPC")
				{
					//sensor hit a car, either change lanes or slow down
					TrafficParams closeCarParams = hit.collider.gameObject.GetComponent<TrafficParams>();

					//how much slower is the car going? Is the car crashed?
					if (Mathf.Abs(closeCarParams.speed - carParams.speed) >  1.8f || closeCarParams.crashed)
					{
						//quite a bit slower, go around
						if (canChangeLanes)
						{
							ChooseNewLane();
						}
						else
						{
							//changed lanes too recently, just slow down now
							AdjustToSameSpeed(closeCarParams.speed);
						}
					}
					else
					{
						//not going that much slower, just match the speeds and slow down
						AdjustToSameSpeed(closeCarParams.speed);
					}

				}
				//ray saw a pothole
//				else if (hit.collider.gameObject.tag == "Pothole")
//				{
//					//sensor hit a pothole, better change lanes
//					ChooseNewLane();
//				}
			}

			yield return new WaitForSeconds(scanRate);
		}
		
	}

	void AdjustToSameSpeed(float newSpeed)
	{
		carParams.speed = newSpeed;
		speed = carParams.speed;
	}

	public void ChooseNewLane ()
	{
		if (carParams.laneNum > 1)
		{
			//this car is on the same side as the player
			if (carParams.laneNum == 2)
			{
				StartChangeLanes(3);
			}
			else
			{
				StartChangeLanes(2);
			}
		}
		else
		{
			//this car is on the opposite side as the player
			if (carParams.laneNum == 1)
			{
				StartChangeLanes(0);
			}
			else
			{
				StartChangeLanes(1);
			}
		}
	}

	void StartChangeLanes (int newLaneNum)
	{
		//change the lane number for vehicle so that it will start to correct towards it and change state to 'changing lanes'
		carParams.laneNum = newLaneNum;
		lanePos = carParams.FindLanePos();
		lanePos.z = transform.position.z;
		carState = state.changingLanes;
	}

	/*
	void CrashedBehavior()
	{
		rb.AddForce(-transform.forward * (globalSpeed * 200) * Time.deltaTime);
	}
	*/
	void Crashed()
	{
		if (GameManager.instance != null) 
		{
			//crashed, appear to stop by moving the same speed as environment
			if (GameManager.instance.GetComponent<MovementManager>() != null)
			{
				speed = GameManager.instance.GetComponent<MovementManager>().movementSpeed;

			}
		}
		GetComponent<TrafficParams>().smoke.Play();

		carState = state.crashed;
		carParams.crashed = true;
	}

	public void GameOver()
	{
		if (carState != state.crashed)
		{
			carState = state.gameOver;
			//if car is on same side, move it in reverse to make it look like it was moving forward
			if (sameSide == -1)
			{
				sameSide = 1;
			}
		}
		else
		{
			//if it's crashed during a game over, just send it to init so that it won't move
			carState = state.init;
		}
	}
	
	void OnCollisionEnter(Collision col)
	{
		if (carState != state.crashed)
		{

			if (col.gameObject.tag == "Player")
			{
				Crashed();
			}

			else if(col.gameObject.tag == "NPC" && canCrash == true)
			{
				Crashed();
			}

			print("An NPC just hit: " + col.gameObject);

		}
	}

	private void OnDisable()
	{
		if (sameSide == -1)
		{
			speed = carParams.ssSpeedMin;
		}
		else
		{
			speed = carParams.osSpeedMin;
		}
	}
	/*
	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "Pothole")
		{
			if (hitPothole < 4)
			{
				speed = GetComponent<TrafficParams>().HitPothole();
				if (speed <= 1)
				{
					Crashed();
				}
				hitPothole++;
			}
			else
			{
				Crashed();
			}
		}

	}*/
}
