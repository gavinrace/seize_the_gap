﻿using System.Collections;
using UnityEngine;

public class VehicleModParams : MonoBehaviour {

	[Header("Color Changes")]
	//public int mainColorIndex;
	//public int accentColorIndex;
	//public List<Color> availableMainColors;
	//public List<Color> availableAccentColors;
	public VertexColorChooser mainMesh;
	public VertexColorChooser accentMesh;

	[Header("Add-ons")]
	public Transform topperPos;
	//public Transform accessoryPos;

	[Header("Performance")]
	public int steering;
	[Tooltip("Range from 0.1 - 1. Smaller = Stronger")]
	public float strength;
	public string vehicleName;

}
