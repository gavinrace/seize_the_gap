// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.4313726,fgcg:0.5019608,fgcb:0.5450981,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:9361,x:34306,y:32938,varname:node_9361,prsc:2|custl-380-OUT,olwid-6195-OUT;n:type:ShaderForge.SFN_Multiply,id:380,x:34107,y:33181,varname:node_380,prsc:2|A-7568-OUT,B-4570-RGB,C-4116-OUT;n:type:ShaderForge.SFN_Slider,id:6195,x:33998,y:33500,ptovrint:False,ptlb:Outline,ptin:_Outline,varname:node_6195,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.05,max:1;n:type:ShaderForge.SFN_LightAttenuation,id:7568,x:33912,y:33044,varname:node_7568,prsc:2;n:type:ShaderForge.SFN_LightColor,id:4570,x:33912,y:33170,varname:node_4570,prsc:2;n:type:ShaderForge.SFN_Multiply,id:4116,x:33661,y:33209,varname:node_4116,prsc:2|A-2002-RGB,B-6791-OUT;n:type:ShaderForge.SFN_VertexColor,id:2002,x:33478,y:33047,varname:node_2002,prsc:2;n:type:ShaderForge.SFN_Posterize,id:6791,x:33422,y:33246,varname:node_6791,prsc:2|IN-3711-OUT,STPS-606-OUT;n:type:ShaderForge.SFN_ValueProperty,id:606,x:33196,y:33307,ptovrint:False,ptlb:Steps,ptin:_Steps,varname:node_606,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:8;n:type:ShaderForge.SFN_Dot,id:3711,x:32953,y:33147,varname:node_3711,prsc:2,dt:1|A-492-OUT,B-2841-OUT;n:type:ShaderForge.SFN_LightVector,id:492,x:32706,y:33147,varname:node_492,prsc:2;n:type:ShaderForge.SFN_NormalVector,id:2841,x:32706,y:33276,prsc:2,pt:True;n:type:ShaderForge.SFN_Add,id:2597,x:33912,y:33314,varname:node_2597,prsc:2|A-4116-OUT;proporder:6195-606;pass:END;sub:END;*/

Shader "Shader Forge/Custom" {
    Properties {
        _Outline ("Outline", Range(0, 1)) = 0.05
        _Steps ("Steps", Float ) = 8
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "Outline"
            Tags {
            }
            Cull Front
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles metal 
            #pragma target 3.0
            uniform float _Outline;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                UNITY_FOG_COORDS(0)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.pos = UnityObjectToClipPos( float4(v.vertex.xyz + v.normal*_Outline,1) );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                return fixed4(float3(0,0,0),0);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles metal 
            #pragma target 3.0
            uniform float _Steps;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                float4 vertexColor : COLOR;
                LIGHTING_COORDS(2,3)
                UNITY_FOG_COORDS(4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 node_4116 = (i.vertexColor.rgb*floor(max(0,dot(lightDirection,normalDirection)) * _Steps) / (_Steps - 1));
                float3 finalColor = (attenuation*_LightColor0.rgb*node_4116);
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
