﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeatherManager : MonoBehaviour {

	public List<Renderer> weatherObjects;
	public Material globalMaterial;
	public float maxGloss;
	public float maxSnow;
	public List<Collider>targetPhyMats;
	
	public static WeatherManager instance;
	float transVal;
	public float glossAmount;
	public float snowAmount;

	public CarManager player;
	
	public enum weather
	{
		sunny,
		raining,
		snowing
	};

	public weather gameWeather;


	[Header("Weather Particles")]
	public ParticleSystem rainParticles;
	public ParticleSystem trickleRainParticles;
	public ParticleSystem snowParticles;
	public ParticleSystem clouds;

	[Header("Environment Colors")]
	public Camera mainCam;
	public Color sunnySkyColor;
	public Color rainySkyColor;
	public Color snowySkyColor;
	public Color sunnyAmbientColor;
	public Color rainyAmbientColor;
	public Color snowyAmbientColor;


	// Use this for initialization
	void Start () {
		instance = this;
	}

	private void OnEnable()
	{
		StartCoroutine(ResetMat());
	}

	IEnumerator ResetMat ()
	{
		yield return new WaitForEndOfFrame();
		globalMaterial.SetFloat("_Gloss", 0);
		globalMaterial.SetFloat("_SnowAmount", 0);
	}

	public void WeatherSwitch ()
	{
		
		if (gameWeather == weather.sunny)
		{
			StartCoroutine( WeatherTransition(weather.raining) );
			print("should change drag");

			//player.AlterDrivingfromWeather(0);
			AlterPhyMats(0.01f);
			trickleRainParticles.gameObject.SetActive(true);
			rainParticles.gameObject.SetActive(true);
			//trickleRainParticles.Play();
			//rainParticles.Play();
		}
		else
		{
			StartCoroutine( WeatherTransition(weather.snowing) );
			print("should change drag");

			//player.AlterDrivingfromWeather(1);
			AlterPhyMats(0f);
			rainParticles.gameObject.SetActive(false);
			snowParticles.gameObject.SetActive(true);
			//rainParticles.Stop();
			//snowParticles.Play();
		}
	}

	void AlterPhyMats (float val)
	{
		foreach (var col in targetPhyMats) 
		{
			col.material.dynamicFriction = val;
		}
	}

	IEnumerator WeatherTransition (weather theWeather)
	{
		//wait a few seconds for the particles to play before affecting the look of the scene
		yield return new WaitForSeconds(5);
		transVal = 0;
		gameWeather = theWeather;
		Color skyColor;
		Color cloudEndColor;
		Color ambientColor;

		cloudEndColor = clouds.colorOverLifetime.color.colorMax;

		if (gameWeather == weather.raining) {
			while (transVal < 1) 
			{
				//increment
				transVal += 0.005f;

				if (transVal <= maxGloss)
				{
					//change the gloss value that newly spawned objects will reference
					//glossAmount = transVal;
					globalMaterial.SetFloat("_Gloss", transVal);
				}
				
				//fade sky color and ambient color
				skyColor = Color.Lerp(sunnySkyColor, rainySkyColor, transVal);

				//fade cloud start color
				var col = clouds.colorOverLifetime;
				Gradient grad = new Gradient();
				grad.SetKeys(new GradientColorKey[] { new GradientColorKey(skyColor, 1.0f), new GradientColorKey(cloudEndColor, 1.0f) }, new GradientAlphaKey[] { new GradientAlphaKey(1.0f, 0.0f), new GradientAlphaKey(1.0f, 1.0f) });

				col.color = grad;

				//apply sky color to camera
				mainCam.backgroundColor = skyColor;

				ambientColor = Color.Lerp(sunnyAmbientColor, rainyAmbientColor, transVal);
				RenderSettings.ambientLight = ambientColor;
				

				yield return new WaitForSeconds(0.1f);
			}
			
		} 
		else {
			float oppoVal = maxGloss;
			while (transVal < 1) 
			{
				transVal += 0.005f;
				oppoVal -= 0.005f;

				//start subtracting from the gloss until it hits 0
				if (oppoVal > 0)
				{
					glossAmount = oppoVal;
					globalMaterial.SetFloat("_Gloss", oppoVal);

				}

				if (transVal <= maxSnow)
				{
					//change the snow value
					snowAmount = transVal;

					//apply to global material
					globalMaterial.SetFloat("_SnowAmount", transVal);
				}

				//fade sky color and ambient color
				skyColor = Color.Lerp(rainySkyColor, snowySkyColor, transVal);
				mainCam.backgroundColor = skyColor;

				ambientColor = Color.Lerp(rainyAmbientColor, snowyAmbientColor, transVal);
				RenderSettings.ambientLight = ambientColor;

				

				yield return new WaitForSeconds(0.1f);
			}
				//make sure the snow starts
				//snowParticles.Play();

		}

	}

}
