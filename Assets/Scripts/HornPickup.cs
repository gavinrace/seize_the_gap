﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HornPickup : MonoBehaviour
{

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Finish")
		{
			other.transform.parent.parent.parent.GetComponent<Horn>().PickedUp();

			transform.parent.gameObject.SetActive(false);
		}
	}
}
