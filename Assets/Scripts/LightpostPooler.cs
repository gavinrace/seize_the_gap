﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightpostPooler : MonoBehaviour {

	public GameObject physicsLightpole;
	public List<GameObject> lightpolePool = new List<GameObject>();

	public GameObject GetPooledLightpole ()
	{
		for (int i = 0; i < lightpolePool.Count; i++)
			{
			if (lightpolePool[i].activeInHierarchy == false)
				{
					//print("found inactive car to use");
				lightpolePool[i].SetActive(true);
				return lightpolePool[i];
				}
				else {
					//print("car is active");
				}
			}
			//print("went through whole list and found no inactive cars");
			//none left
			GameObject newPole = GameObject.Instantiate(physicsLightpole);
			lightpolePool.Add(newPole);
			//MovementManager.instance.movingObjects.Add(newPothole);
			return newPole;
	}
}
