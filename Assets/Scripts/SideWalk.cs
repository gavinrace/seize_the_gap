﻿using UnityEngine;

public class SideWalk : MonoBehaviour {

	public bool moving;

	void FixedUpdate()
	{
		if (moving)
		{
			//print("MOVE");
			transform.Translate(-transform.forward * Time.deltaTime * MovementManager.instance.movementSpeed);
		}
		

	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Resetter")
		{
			moving = false;
		}
	}
}
