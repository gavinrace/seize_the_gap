﻿using UnityEngine;
using UnityEngine.UI;

public class Horn : MonoBehaviour {

	public int maxHonks;
	public int honks;

	public GameObject hornButton;
	public Transform hornObject;

	public void PickedUp ()
	{
		honks = maxHonks;

		hornButton.SetActive(true);
		hornButton.GetComponentInChildren<Text>().text = honks.ToString();
	}

	public void HornHonked()
	{
		honks--;
		if (honks > 0)
		{
			HonkHorn();
		}
		else
		{
			HonkHorn();
			DisableHorn();
		}
	}

	void HonkHorn ()
	{
		RaycastHit hit;

		//Ray for detectin gother cars
		Debug.DrawRay(hornObject.position, hornObject.forward * 80, Color.green, 0.5f);
		if (Physics.Raycast(hornObject.position, hornObject.forward, out hit, 80))
		{
			print("horn hit something: " + hit.collider.gameObject);
			if (hit.collider.gameObject.tag == "NPC")
			{
				GameObject carInTheWay = hit.collider.gameObject;
				TrafficAi carInTheWayAI = carInTheWay.GetComponent<TrafficAi>();

				//make sure car is active
				if (carInTheWayAI.carState != TrafficAi.state.crashed)
				{
					//make car change lanes
					carInTheWayAI.ChooseNewLane();
				}

			}
		}

		hornButton.GetComponentInChildren<Text>().text = honks.ToString();

	}


	void DisableHorn()
	{
		hornButton.SetActive(false);
	}
}
