﻿using UnityEngine;

public class PointPickup : MonoBehaviour {

	public int value;

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Finish")
		{
			GameManager.instance.UpdateScore(value);

			gameObject.SetActive(false);
		}
	}
}
