﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ScreenFader : MonoBehaviour {

	public List<Image> overlays;
	public bool startBlack;
	public bool mainMenu;

	// Use this for initialization
	void Start () {
		foreach (var image in overlays) {
			image.gameObject.SetActive(true);
		}

		if (!startBlack) {
			foreach (var image in overlays) 
			{
				image.canvasRenderer.SetAlpha(0.0f);
			}
	
		}
		else{
			foreach (var image in overlays) 
			{
				image.canvasRenderer.SetAlpha(1f);
			}
			FadeScreen(true);
		}


	}

	public void FadeScreen (bool fadeIn)
	{
		//make sure fader image is enabled
		foreach (var image in overlays)
		{
			if (!image.gameObject.activeInHierarchy)
			{
				image.gameObject.SetActive(true);
			}
		}

		if (fadeIn) {
		print("fade in");
			foreach (var image in overlays) 
			{
				image.CrossFadeAlpha(0, 1, false);
			}
		}
		else{
			print("fade out");
			foreach (var image in overlays) 
			{
				image.CrossFadeAlpha(1.0f, 1, false);
			}
			if(mainMenu)
			{
				MainMenuAnimation();
			}
		}
	}

	void MainMenuAnimation ()
	{
		GetComponent<Animator>().SetBool("LoadLevel", true);
	}

	void DeactivateFader()
	{
		foreach (var image in overlays)
		{
			image.gameObject.SetActive(false);
		}
	}
}
