﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolMember : MonoBehaviour {

	public ObjectPooler pool;

	void OnDisable ()
	{
		pool.NextObject = gameObject;
	}
}
