﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TiresPickup : MonoBehaviour {

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Finish")
		{
			other.transform.parent.parent.parent.GetComponent<TiresManager>().PickedUpProtection();

			transform.parent.gameObject.SetActive(false);
		}
	}
}
