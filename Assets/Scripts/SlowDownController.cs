﻿using System.Collections;
using UnityEngine;

public class SlowDownController : MonoBehaviour
{

	public float slowDownTime;
	float slowDownTimer;

	void Start()
	{	
		Time.timeScale = 1;
		Time.fixedDeltaTime = 0.02f;
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "NPC")
		{
			slowDownTimer = slowDownTime;
			StartCoroutine(Timer());
			StartCoroutine( SlowDown() );
		}
	}

	//private void OnTriggerExit(Collider other)
	//{
	//	if (other.gameObject.tag == "NPC")
	//	{
	//		StartCoroutine( SpeedUp() );
	//	}
	//}

	IEnumerator SlowDown()
	{
		while (Time.timeScale > 0.5f)
		{
			Time.timeScale -= 0.05f;
			Time.fixedDeltaTime = 0.02f * Time.timeScale;
			yield return new WaitForEndOfFrame();
		}
		//Time.timeScale = 0.5f;
		//Time.fixedDeltaTime = 0.02f * Time.timeScale;
	}

	IEnumerator Timer()
	{
		while (slowDownTimer > 0)
		{
			slowDownTimer -= 0.1f;
			yield return new WaitForSeconds(0.1f);
		}
		StartCoroutine(SpeedUp());
	} 

	IEnumerator SpeedUp()
	{
		while (Time.timeScale < 1)
		{
			Time.timeScale += 0.05f;
			Time.fixedDeltaTime = 0.02f * Time.timeScale;
			yield return new WaitForEndOfFrame();
		}
		//Time.timeScale = 1;
		//Time.fixedDeltaTime = 0.02f;
	}
}
