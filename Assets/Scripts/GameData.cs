﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameData
{
	//Game data
	public int stylePoints;
	public string masterChallengeList = "Hit 3 potholes.!hitPotholes!3!none!0!200!Didn't like these tires anyway.&Drive on the wrong side for thirty seconds in the first 3 miles.!otherside!600!distanceLess!3!1000!Off to a terrifying start.&Drive for two miles.!distance!2!none!0!200!Pssh, two miles is no problem.&Have more than half health by the time it starts raining.!level!3!healthMore!50!500!This car's still kicking!&Drive on the wrong side for 20 seconds.!otherside!200!none!0!200!Lane lines are just a suggestion.&Drive for five miles.!distance!5!none!0!500!Making my way downtown...&Drive on the wrong side for 90 seconds.!otherside!900!none!0!500!Adrenaline junkie!&Drive until it starts raining.!level!3!none!0!750!Things are going to get worse...&Drive for ten miles.!distance!10!none!0!1000!What wonderful weaving!&Drive until it starts snowing.!level!4!none!0!1500!Where am I even going? Brrr.";
	public string playerChallengeList;

	public int currentPrize;

	//Player car info
	public int playerVehicle;
	//public List<int> purchasedCars;
	public int playerCarMainColor;
	//public List<int> purchasedMainColors;
	public int playerCarAccentColor;
	//public List<int> purchasedAccentColors;
	public int playerTopper;

	public bool boughtHorn;

	public bool boughtTires;

	public bool boughtMoneyStack;

	public bool boughtHealth;

	public List<int> purchasedvehicles;
	//public int playerAccessory;
	//public List<int> purchasedAccessories;
	public int maxSavedSP;

	public float maxDistance;

	public int frameRate;

	public bool firstTime;
}
