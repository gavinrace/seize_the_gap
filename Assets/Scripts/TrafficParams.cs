﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrafficParams : MonoBehaviour {

	public ParticleSystem smoke;
	public bool sameSide;
	public float globalSpeed;
	float baseSpeed;
	public float speed;
	public float ssSpeedMin;
	public float ssSpeedMax;
	public float osSpeedMin;
	public float osSpeedMax;
	public int laneNum;
	public Vector3 lanePos;

	public bool crashed;

	void OnEnable ()
	{
		globalSpeed = MovementManager.instance.movementSpeed;
	}

	void OnDisable ()
	{
		smoke.Stop();
	}

	public bool ChooseSide()
	{
		if (Random.Range(0,100) > 50)
		{
			sameSide = true;
		}
		else {
			sameSide = false;
		}

		return sameSide;
	}

	public void ChooseSpeed()
	{
		if (sameSide)
		{
			baseSpeed = Random.Range(ssSpeedMin, ssSpeedMax);
		}
		else{
			baseSpeed = Random.Range(osSpeedMin, osSpeedMax);
		}
		speed = baseSpeed * globalSpeed;
	}

	public float HitPothole()
	{
		float newSpeed = speed;
		//hit pothole, slow the car down a bit. This will probably cause more traffic issues. Good.
		if (sameSide)
		{
			newSpeed += (ssSpeedMax - ssSpeedMin) / 3;
		}
		else{
			newSpeed -= (osSpeedMin - osSpeedMax) / 3;
		}

		return newSpeed;

		//print(gameObject.name + " hit a pothole. Speed was " + origSpeed + " and now it is " + speed);
	}

	public Vector3 FindLanePos ()
	{
		Vector3 pos = TrafficManager.instance.lanes[laneNum].position;
		return pos;
	}


}
