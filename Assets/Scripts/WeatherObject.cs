﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeatherObject : MonoBehaviour {

	float glossAmount;
	float snowAmount;
	public List<int> targetMatIndices;
	public Renderer objRenderer;

	private void OnEnable()
	{

		glossAmount = WeatherManager.instance.glossAmount;
		snowAmount = WeatherManager.instance.snowAmount;

		foreach (int index in targetMatIndices) {
			objRenderer.materials[index].SetFloat("_Gloss", glossAmount);
			objRenderer.materials[index].SetFloat("_SnowAmount", snowAmount);
		}

	}
}
