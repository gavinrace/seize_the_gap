﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarAI : MonoBehaviour {

	public Transform carSensor;
	public Transform potholeSensor;
	public float scanRate;
	public GameObject hitCar;
	Color origColor;
	//public bool shouldChangeLanes;
	//public bool crashed;
	private int hitPothole;
	private Rigidbody rb;
	public Transform currentLane;

	private float laneDiff;

	//pulled from params
	private TrafficParams tParams;
	private float speed;
	private float globalSpeed;

	public enum state
	{
		driving,
		changingLanes,
		crashed,
		gameOver
	};

	public state carState;

	void Start ()
	{
		rb = GetComponent<Rigidbody>();
		tParams = GetComponent<TrafficParams>();
		origColor = GetComponent<Renderer>().material.GetColor("_MainColor");
		currentLane = TrafficManager.instance.lanes[GetComponent<TrafficParams>().laneNum];

	}


	// Use this for initialization
	void OnEnable () {
		
		StartCoroutine(ScanAhead());

		//shouldChangeLanes = false;
		//crashed = false;

		carState = state.driving;

		speed = 1;
		globalSpeed = 1;

		StartCoroutine(FindParamsDelay());
	}

	IEnumerator FindParamsDelay ()
	{
		yield return new WaitForSeconds(0.1f);
		//speed = GetComponent<TrafficParams>().speed;
		currentLane = TrafficManager.instance.lanes[GetComponent<TrafficParams>().laneNum];

		carState = state.driving;

		speed = tParams.speed;
		globalSpeed = tParams.globalSpeed;
	}	
	// Update is called once per frame
	void Update () {

		//find how far away from center of lane the car is
		laneDiff = Mathf.Abs(transform.position.x - currentLane.position.x);

		switch (carState) 
		{
			case state.driving:
				Driving();
				break;

			case state.changingLanes:
				Driving();
				break;

			case state.crashed:
				CrashedBehavior();
				break;
			case state.gameOver:
				GameOverBehavior();
				break;
			default:
				break;
		}



		

	}

	

	void Driving ()
	{
		rb.AddForce(-transform.forward * (globalSpeed * 200 / speed) * Time.deltaTime);

		if ( laneDiff > 0.1f)
		{
			//GetComponent<Renderer>().material.color = Color.white;
			CorrectDriving();
		}
		else if (carState == state.changingLanes) 
		{
			carState = state.driving;
			hitCar = null;
		}
	}

	IEnumerator ScanAhead ()
	{
		while (true)
		{
			

			Vector3 fwd = carSensor.transform.forward;
			RaycastHit hit;

			//Ray for detectin gother cars
			Debug.DrawRay(carSensor.position, fwd * 20, Color.green, 0.5f);
			if (Physics.Raycast(carSensor.position, fwd, out hit, 20)) 
	        {
		        if (hit.collider.gameObject.tag == "NPC")
		        {
					//GetComponent<Renderer>().material.color = Color.magenta;
					GameObject carInTheWay = hit.collider.gameObject;
					hitCar = carInTheWay;
					TrafficParams carInTheWayParams = carInTheWay.GetComponent<TrafficParams>();
					CarAI carInTheWayAI = carInTheWay.GetComponent<CarAI>();
		            //print("Car " + carInTheWay + " is in front of car" + gameObject.name + "!");
		            if (carInTheWayAI.carState == state.driving)
		            {
		            	carInTheWayAI.carState = state.changingLanes;
			           // carInTheWayAI.shouldChangeLanes = true;
			            if (carInTheWayParams.sameSide)
			            {
			            	//if their speed difference is close, they will match, otherwise, it will go around.
			            	if (Mathf.Abs(carInTheWayParams.speed - GetComponent<TrafficParams>().speed) > (carInTheWayParams.ssSpeedMax - carInTheWayParams.ssSpeedMin) / 4)
			            	{
			            		//shouldChangeLanes = true;
					            if (carInTheWayParams.laneNum == 2)
					            {
					            	carInTheWay.GetComponent<CarAI>().StartLaneChange(3);
					            }
					            else{
									carInTheWay.GetComponent<CarAI>().StartLaneChange(2);
					            }
				            }
				            else{
				            	GetComponent<TrafficParams>().speed = carInTheWayParams.speed;
				            }
				        }

			            else if (carState != state.crashed)
			            {
							//if their speed difference is close, they will match, otherwise, it will go around.
							if (Mathf.Abs(carInTheWayParams.speed - GetComponent<TrafficParams>().speed) > (carInTheWayParams.osSpeedMin - carInTheWayParams.osSpeedMax) / 4)
			            	{
								if (carInTheWayParams.laneNum == 0)
					            {
									StartLaneChange(1);
					            }
					            else{
									StartLaneChange(0);
					            }
					        }
				         }		
				     }            
		        }
		    }

		    //Scan for potholes
			Vector3 rev = potholeSensor.transform.forward;
			RaycastHit hit2;

			//Ray for detecting potholes
			Debug.DrawRay(potholeSensor.position, rev * 20, Color.cyan, 0.5f);
			if (Physics.Raycast(potholeSensor.position, rev, out hit2, 20)) 
	        {
				carState = state.changingLanes;
		        if (hit2.collider.gameObject.tag == "Pothole")
		        {
					FindNewLane();
		        }
		    }

			yield return new WaitForSeconds(scanRate);
			GetComponent<Renderer>().material.color = origColor;

		}
	}

	public void FindNewLane()
	{
		if (GetComponent<TrafficParams>().laneNum == 0)
		{
			StartLaneChange(1);
		}
		else if (GetComponent<TrafficParams>().laneNum == 1)
		{
			StartLaneChange(0);
		}
		else if (GetComponent<TrafficParams>().laneNum == 2)
		{
			StartLaneChange(3);
		}
		else if (GetComponent<TrafficParams>().laneNum == 3)
		{
			StartLaneChange(2);
		}
	}

	public void StartLaneChange(int newLane)
	{
		//StartCoroutine(ChangeLanes(newLane));
		GetComponent<TrafficParams>().laneNum = newLane;
		currentLane = TrafficManager.instance.lanes[GetComponent<TrafficParams>().laneNum];
	}

	void CorrectDriving()
	{
		if (carState != state.crashed)
		{
			rb.AddForce((new Vector3(currentLane.position.x, transform.position.y, transform.position.z) - transform.position).normalized * 500 * Time.smoothDeltaTime);
		}

	}

	IEnumerator ChangeLanes(int newLane)
	{
		//print(gameObject.name + " is changing from lane " + GetComponent<TrafficParams>().laneNum + " to lane " + newLane);

		while(Mathf.Abs(transform.position.x - TrafficManager.instance.lanes[newLane].position.x) > 0.1f)
		{
			//print(gameObject.name + " is changing from lane " + GetComponent<TrafficParams>().laneNum + " to lane " + newLane);
			//GetComponent<Renderer>().material.color = Color.cyan;
			//MAKE PHYSICS
			rb.AddForce(Mathf.MoveTowards(transform.position.x, TrafficManager.instance.lanes[newLane].position.x, Time.deltaTime), 0,0);
			//float newX = Mathf.MoveTowards(transform.position.x, TrafficManager.instance.lanes[newLane].position.x, Time.deltaTime * GetComponent<TrafficParams>().speed); 
			//Vector3 newPos = new Vector3 (newX, transform.position.y, transform.position.z);
			//transform.position = newPos;
			yield return new WaitForEndOfFrame();
		}
		//shouldChangeLanes = false;
		GetComponent<TrafficParams>().laneNum = newLane;
		GetComponent<Renderer>().material.color = origColor;
	}

	void CrashedBehavior()
	{
		rb.AddForce(-transform.forward * (globalSpeed * 200) * Time.deltaTime);
	}

	void Crashed()
	{
		//crashed, appear to stop by moving the same speed as environment
		speed = 1;
		GetComponent<TrafficParams>().smoke.Play();
		carState = state.crashed;
	}


	void GameOverBehavior()
	{
		if (carState != state.crashed)
		{
			rb.AddForce(-transform.forward * (-globalSpeed * 200 / speed) * Time.deltaTime);
		}
		
	}

	public void GameOver ()
	{
		carState = state.gameOver;
	}

	void OnCollisionEnter (Collision col)
	{
		if (col.gameObject.tag == "NPC" || col.gameObject.tag == "Player")
		{	
			//print("CRASH");
			//shouldChangeLanes = false;
			Crashed();
			//crashed = true;
		}

	}

	void OnTriggerEnter (Collider col)
	{
		if (col.gameObject.tag == "Pothole")
		{
			if (hitPothole < 4)
			{
				speed = GetComponent<TrafficParams>().HitPothole();
				if (speed <= 1)
				{
					Crashed();
				}
				hitPothole++;
			}
			 else {
				Crashed();
			}
		}

	}
}
