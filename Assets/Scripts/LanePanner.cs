﻿using UnityEngine;
using System.Collections;

public class LanePanner : MonoBehaviour
{
	public Transform otherLanes;

	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "Resetter")
		{
			transform.position = new Vector3(transform.position.x, transform.position.y, otherLanes.position.z + 180);
		}
	}
}