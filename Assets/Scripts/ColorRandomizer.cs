﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorRandomizer : MonoBehaviour {

	public Renderer objRenderer;
	public List<int> matIndex;
	private Material colorableMat;
	public Color[] colors;


	void OnEnable ()
	{
		ChooseColor();
	}

	void ChooseColor()
	{
		
		int colorInt;

		foreach (var indexOfMat in matIndex) {

		colorInt = Random.Range(0, colors.Length);

		colorableMat = objRenderer.materials[indexOfMat];
		colorableMat.SetColor("_MainColor", colors[colorInt]);
		}
		 		
	}
}
