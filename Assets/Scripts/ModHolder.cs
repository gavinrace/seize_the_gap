﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModHolder : MonoBehaviour {

	public static ModHolder instance;

	[Header("Color Changes")]
	public List<Color> availableMainColors;
	public List<Color> availableAccentColors;
	public List<int> mainColorPriceList;
	public List<int> accentColorPriceList;

	[Header("Add-ons")]
	public List<GameObject> availableVehicles;
	public List<int> vehiclePriceList;
	public List<GameObject> availableToppers;
	public List<int> topperPriceList;
	//public List<GameObject> availableAccessories;
	//public List<int> accessoryPriceList;

	[Header("Utilities")]
	public List<int> utilitiesPriceList;

	public void Awake()
     {
         DontDestroyOnLoad(this);
 
         if (FindObjectsOfType(GetType()).Length > 1)
         {
             Destroy(gameObject);
         }
     }

	// Use this for initialization
	void Start () {
		instance = this;
		//DontDestroyOnLoad(this);
			
	}

}
