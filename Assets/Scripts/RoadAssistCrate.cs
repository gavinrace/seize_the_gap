﻿using TMPro;
using UnityEngine;

public class RoadAssistCrate : MonoBehaviour {

	public int cost;
	public TextMeshPro costText;
	public PickupsManager pm;
	public GameObject mesh;
	bool destroyed;

	// Use this for initialization
	void Start () {

		costText.text = "-$" + cost;
		Invoke("CheckIfUnlocked",0.1f);
	}

	void CheckIfUnlocked()
	{
		if (FileManager.instance.currentGameData.currentPrize > 0)
		{
			CheckIfEnoughMoney();
		}
		else
		{
			gameObject.SetActive(false);
		}
	}

	private void OnTriggerEnter(Collider other)
	{
		print(other + "hit crate");
		if (other.gameObject.tag == "Finish" && destroyed == false)
		{
			destroyed = true;
			BreakCrate();
			ConfirmPurchase();
		}
		
	}

	void CheckIfEnoughMoney()
	{
		print("current cash is: " + StylePointHandler.instance.readableStylePoints);
		if (StylePointHandler.instance.readableStylePoints < cost)
		{
			gameObject.SetActive(false);
		}

		//also check if it has been unlocked
		
	}

	void ConfirmPurchase()
	{
		if (StylePointHandler.instance.SpendPoints(cost) == 1)
		{
			//points spent successfully, initiate pickups
			SelectRandomPickup();
			costText.text = "SUCCESS!";
		}
		else if (StylePointHandler.instance.SpendPoints(cost) == 0)
		{
			//this should be impossible because of the initial money check
			print("somehow there are not enough point to spawn pickups. Do nothing.");
		}
		else if (StylePointHandler.instance.SpendPoints(cost) == 3)
		{
			print("someone is either cheating or something is broken");
		}
	}

	void SelectRandomPickup()
	{
		int randNum;
		int chosenNum;
		//the three here is the number of available pickip types. Increase if more pickups are added
		randNum = Random.Range(0, 3 * 10);
		randNum = randNum / 10;
		chosenNum = Mathf.RoundToInt(randNum);

		pm.AddPickup(chosenNum);
	}

	void BreakCrate()
	{
		mesh.SetActive(false);
	}
}
