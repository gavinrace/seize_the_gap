﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PotholeManager : MonoBehaviour {

	public List<GameObject> potholes;
	public List<GameObject> potholePool;
	public float initialDelay;
	public float minDelay;
	public float maxDelay;
	public float randPos;

	public bool gameOver;


	// Use this for initialization
	void Start () {
		StartCoroutine(SpawnPothole());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator SpawnPothole ()
	{
		//initial delay
		yield return new WaitForSeconds(initialDelay);

		while(!gameOver)
		{
			float delay = Random.Range(minDelay,maxDelay);
			yield return new WaitForSeconds(delay);

			GameObject pHole = GetPooledPothole();
			pHole.transform.position = new Vector3(Random.Range(-randPos, randPos), 0.09f, 100);
		}
	}

	GameObject GetPooledPothole ()
	{
		for (int i = 0; i < potholePool.Count; i++)
			{
			if (potholePool[i].activeInHierarchy == false)
				{
					//print("found inactive car to use");
				potholePool[i].SetActive(true);
				return potholePool[i];
				}
				else {
					//print("car is active");
				}
			}
			//print("went through whole list and found no inactive cars");
			//none left
			GameObject newPothole = GameObject.Instantiate(potholes[Random.Range(0, potholes.Count)]);
			potholePool.Add(newPothole);
			MovementManager.instance.movingObjects.Add(newPothole);
			return newPothole;
	}
}
