﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour {

	public GameObject projectile;
	public List<GameObject> projectilePool;
	public float force;

	public void Shoot()
	{
		GameObject obj = PooledObj();
		obj.GetComponent<Rigidbody>().AddForce(transform.forward * force);
	}

	GameObject PooledObj()
	{
		for (int i = 0; i < projectilePool.Count; i++)
		{
			if (projectilePool[i].activeInHierarchy == false)
			{
				//print("found inactive point obj to use");
				projectilePool[i].SetActive(true);
				return projectilePool[i];
			}
			else
			{
				//print("point obj is active");
			}
		}
		//print("went through whole list and found no inactive point objs);
		//none left
		GameObject newObj = GameObject.Instantiate(projectile);
		projectilePool.Add(newObj);
		return newObj;
	}
}
