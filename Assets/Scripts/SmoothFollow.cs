﻿using UnityEngine;
     using System.Collections;
     
     public class SmoothFollow : MonoBehaviour {
         
         // The target we are following
         public Transform target;
         // The distance in the x-z plane to the target
         public float distance = 10.0f;
         // the height we want the camera to be above the target
         public float height = 5.0f;
         // How much we 
         public float speed = 2f;
	public bool lockHeight;
		private float lockedHeight;

	private void Start()
	{
		lockedHeight = target.position.y + height;
	}

	void FixedUpdate ()
	{
             // Early out if we don't have a target
             if (!target) return;

		// Set the position of the camera on the x-z plane to:
		// distance meters behind the target
		if (lockHeight)
		{
			transform.position = Vector3.Lerp(transform.position, new Vector3(target.position.x, lockedHeight/*target.position.y + height*/, target.position.z - distance), Time.deltaTime * speed);
		}
		else
		{
			transform.position = Vector3.Lerp(transform.position, new Vector3(target.position.x, target.position.y + height, target.position.z - distance), Time.deltaTime * speed);

		}
	}

}