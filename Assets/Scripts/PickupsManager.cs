﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupsManager : MonoBehaviour
{

	public List<int> availablePickups;
	public GameObject health;
	public GameObject tires;
	public GameObject horn;
	public GameObject money;
	public bool healthBought;
	public bool tiresBought;
	public bool hornBought;
	public bool moneyBought;
	public List<GameObject> healthPool;
	public List<GameObject> tiresPool;
	public List<GameObject> hornPool;
	public List<GameObject> moneyPool;
	public float minDelay;
	public float maxDelay;

	public bool gameOver;


	// Use this for initialization
	void Start()
	{
		Invoke("EvalPurchasedPickups", 0.04f);
	}

	void EvalPurchasedPickups()
	{
		GameData curData = FileManager.instance.currentGameData;

		availablePickups = new List<int>();

		print("Health: " + curData.boughtHealth + " Tires: " + curData.boughtTires + " Horn: " + curData.boughtHorn + " Money: " + curData.boughtMoneyStack);

		if (curData.boughtHealth) {
			availablePickups.Add(0);
			healthBought = true;
		}
		if (curData.boughtTires) {
			availablePickups.Add(1);
			tiresBought = true;
		}
		if (curData.boughtHorn) {
			availablePickups.Add(2);
			hornBought = true;
		}
		if (curData.boughtMoneyStack) {
			availablePickups.Add(3);
			moneyBought = true;
		}

		if (availablePickups.Count > 0) 
		{
			StartCoroutine(SpawnPickup());
		}

	}

	//crate will use this to add the random pickup to the spawner
	public void AddPickup(int pickup)
	{
		availablePickups.Add(pickup);
		StartCoroutine(SpawnPickup());
	}

	IEnumerator SpawnPickup()
	{
		while (!gameOver)
		{
			float delay = Random.Range(minDelay, maxDelay);
			yield return new WaitForSeconds(delay);

			int lanePos = Random.Range(0, 3);

			GameObject pickup = GetPooledPickup(lanePos);
			pickup.transform.position = new Vector3(TrafficManager.instance.lanes[lanePos].position.x, 0, 200);
		}
	}

	GameObject GetPooledPickup(int dir)
	{

		int ranNum = Random.Range(0, availablePickups.Count);
		print("Random pooled number can range from 0 - " + (availablePickups.Count - 1) + "but is " + ranNum);
		int selectedPickup = availablePickups[ranNum];
		/*switch (dir)
		{
			
			case 0:
				//in first lane from the left. Supports any arrangement except one that goes left.
				if ( ranNum < 33)
				{
					return PooledStraight();
				}
				else if (ranNum > 33 && ranNum < 66)
				{
					return PooledRight();
				}
				else
				{
					return PooledSingle();
				}

			case 1:
				//in one of the middle lanes. Anything will work.
				if (ranNum < 25)
				{
					return PooledStraight();
				}
				else if (ranNum > 25 && ranNum < 50)
				{
					return PooledRight();
				}
				else if (ranNum > 50 && ranNum < 75)
				{
					return PooledLeft();
				}
				else
				{
					return PooledSingle();
				}
			case 2:
				//in one of the middle lanes. Anything will work.
				if (ranNum < 25)
				{
					return PooledStraight();
				}
				else if (ranNum > 25 && ranNum < 50)
				{
					return PooledRight();
				}
				else if (ranNum > 50 && ranNum < 75)
				{
					return PooledLeft();
				}
				else
				{
					return PooledSingle();
				}

			case 3:
				//in far right lanes.Supports anything but right.
				if (ranNum < 33)
				{
					return PooledStraight();
				}
				else if (ranNum > 33 && ranNum < 66)
				{
					return PooledLeft();
				}
				else
				{
					return PooledSingle();
				}

			default:
				//this should never ever happen
				return new GameObject();
		}*/

		if (selectedPickup == 0)
		{
			//spawn health
			return GetPooledHealth();
		}

		else if (selectedPickup == 1)
		{
			//spawn powerup 1
			return GetPooledTires();
		}
		else if (selectedPickup == 2)
		{
			//spawn powerup 2
			return GetPooledHorn();
		}
		else if (selectedPickup == 3)
		{
			//spawn powerup 3
			return GetPooledMoney();
		}
		else
		{
			print("pickups manager is trying to spawn the impossible. Spawning an Empty instead.");
			return new GameObject();
				
		}


	}

	GameObject GetPooledHealth()
	{
		for (int i = 0; i < healthPool.Count; i++)
		{
			if (healthPool[i].activeInHierarchy == false)
			{
				//print("found inactive point obj to use");
				healthPool[i].SetActive(true);
				return healthPool[i];
			}
			else
			{
				//print("point obj is active");
			}
		}
		//print("went through whole list and found no inactive point objs);
		//none left
		GameObject newHealthObj = GameObject.Instantiate(health);
		healthPool.Add(newHealthObj);
		MovementManager.instance.movingObjects.Add(newHealthObj);
		return newHealthObj;
	}
	GameObject GetPooledTires()
	{
		for (int i = 0; i < tiresPool.Count; i++)
		{
			if (tiresPool[i].activeInHierarchy == false)
			{
				//print("found inactive point obj to use");
				tiresPool[i].SetActive(true);
				return tiresPool[i];
			}
			else
			{
				//print("point obj is active");
			}
		}
		//print("went through whole list and found no inactive point objs);
		//none left
		GameObject newObj = GameObject.Instantiate(tires);
		tiresPool.Add(newObj);
		MovementManager.instance.movingObjects.Add(newObj);
		return newObj;
	}

	GameObject GetPooledHorn()
	{
		for (int i = 0; i < hornPool.Count; i++)
		{
			if (hornPool[i].activeInHierarchy == false)
			{
				//print("found inactive point obj to use");
				hornPool[i].SetActive(true);
				return hornPool[i];
			}
			else
			{
				//print("point obj is active");
			}
		}
		//print("went through whole list and found no inactive point objs);
		//none left
		GameObject newObj = GameObject.Instantiate(horn);
		hornPool.Add(newObj);
		MovementManager.instance.movingObjects.Add(newObj);
		return newObj;
	}

	GameObject GetPooledMoney()
	{
		for (int i = 0; i < moneyPool.Count; i++)
		{
			if (moneyPool[i].activeInHierarchy == false)
			{
				//print("found inactive point obj to use");
				moneyPool[i].SetActive(true);
				return moneyPool[i];
			}
			else
			{
				//print("point obj is active");
			}
		}
		//print("went through whole list and found no inactive point objs);
		//none left
		GameObject newObj = GameObject.Instantiate(money);
		moneyPool.Add(newObj);
		MovementManager.instance.movingObjects.Add(newObj);
		return newObj;
	}


}
