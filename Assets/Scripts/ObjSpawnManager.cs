﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjSpawnManager : MonoBehaviour {


	public List<GameObject> commonObj;
	public List<GameObject> rareObj;
	public List<GameObject> commonObjPool;
	public List<GameObject> rareObjPool;
	public string objTag;
	public float spawnOffset;
	public bool reverseDirection;
	[Header("Delay Option")]
	public bool randomDelay;
	public float delayTimeMax;
	[Header("Randomized X Pos Option")]
	public bool randomXPos;
	[Tooltip("Negative 1 for left side, positive 1 for right side.")]
	public int randomXDir;
	public float randomXMax;

	//small edit to file

	void OnTriggerEnter (Collider col)
	{
		if (col.gameObject.tag == objTag)
		{
			if(!randomDelay)
			{
				//print("ojb in trigger");
				SpawnObj(col.gameObject.transform);
			} 
			else {
				StartCoroutine(SpawnDelay(col.gameObject.transform));
			}
		}
	}

	IEnumerator SpawnDelay(Transform obj)
	{
		float delay = Random.Range(0, delayTimeMax);

		yield return new WaitForSeconds(delay);
		SpawnObj(obj);
	}

	void SpawnObj(Transform triggerObj)
	{
		GameObject newObj;
		if (Random.Range(0, 100) > 85)
		{
			//spawn the rarer obj, just to shake things up
			newObj = GetPooledRareObj();
		}
		else
		{
			newObj = GetPooledCommonObj();
		}

		if (randomXPos) {
			float randomX = Random.Range(0,randomXMax);
			randomX *= randomXDir;

			newObj.transform.position = new Vector3(transform.position.x + randomX, 0, transform.position.z + spawnOffset);
		}
		else{
			newObj.transform.position = new Vector3(transform.position.x, 0, transform.position.z + spawnOffset);
			}

		Transform model = newObj.transform.GetChild(0);

		if(reverseDirection)
		{
			model.forward = -transform.forward;
		} else {
			model.forward = transform.forward;
		}


		//update the material when spawning an object 
		//ObjReuser curReuser = triggerObj.GetComponent<ObjReuser>();
		/*if (curReuser.weatherable != null)
		{
			Renderer modelMaterial = curReuser.weatherable;
			int matIndex = curReuser.materialIndx;
			modelMaterial.materials[matIndex].SetFloat("_Gloss", WeatherManager.instance.glossAmount);
			modelMaterial.materials[matIndex].SetFloat("_SnowAmount", WeatherManager.instance.snowAmount);
		}*/
	}

	GameObject GetPooledCommonObj()
	{
		for (int i = 0; i < commonObjPool.Count; i++)
		{
			if (commonObjPool[i].activeInHierarchy == false)
			{
				//print("found inactive car to use");
				commonObjPool[i].SetActive(true);
				return commonObjPool[i];
			}
			else
			{
				//print("car is active");
			}
		}
		//print("went through whole list and found no inactive cars");
		//none left
		GameObject newCommonObj = GameObject.Instantiate(commonObj[Random.Range(0,commonObj.Count)]);
		commonObjPool.Add(newCommonObj);
		MovementManager.instance.movingObjects.Add(newCommonObj);
		return newCommonObj;
	}

	GameObject GetPooledRareObj()
	{
		for (int i = 0; i < rareObjPool.Count; i++)
		{
			if (rareObjPool[i].activeInHierarchy == false)
			{
				//print("found inactive obj to use");
				rareObjPool[i].SetActive(true);
				return rareObjPool[i];
			}
			else
			{
				//print("car is active");
			}
		}
		//print("went through whole list and found no inactive cars");
		//none left
		GameObject newRareObj = GameObject.Instantiate(rareObj[Random.Range(0, rareObj.Count)]);
		rareObjPool.Add(newRareObj);
		MovementManager.instance.movingObjects.Add(newRareObj);
		return newRareObj;
	}
}
