﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FPSDisplay : MonoBehaviour {

	float deltaTime = 0.0f;
	Text display;

	void Start ()
	{
		display = GetComponent<Text>();
	}

	// Update is called once per frame
	void Update () 
	{
		deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
		float msec = deltaTime * 1000.0f;
		float fps = 1.0f / deltaTime;
		string text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);

		display.text = text;
	}
}
