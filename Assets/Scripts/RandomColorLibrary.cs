﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomColorLibrary : MonoBehaviour {

	public List<Color> buildingMain;
	public List<Color> buildingTrim;
	public List<Color> vehicleMain;
	public List<Color> vehicleAccent;

	public static RandomColorLibrary instance;

	void Awake()
	{	
		instance = this;
	}
}
