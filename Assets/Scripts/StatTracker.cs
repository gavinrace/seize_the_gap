﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatTracker : MonoBehaviour
{
	public static StatTracker instance;

	float distance;
	int level;
	int otherside;
	int stylePoints;
	int hitPotholes;
	int hitLightpoles;
	int healthPickedUp;
	int tiresPickedUp;
	int hornPickedUp;
	int moneyPickedUp;
	int carType;
	bool tookDamage;

	bool gameOver = false;
	
	[Tooltip (" STAT GUIDE: 0 = distance 1 = level 2 = otherside 3 = stylePoints(total accumulated points) 4 = hitPotholes 5 = hitLightpoles| x = stat,  y = desired value,    z = index in list")]

	public List<string> challengeDescriptions;
	public List<string> challengeStat;
	public List<string> challengeSecondary;
	public List<int> challengeStatNum;
	public List<int> challengeSecondaryNum;
	public List<int> challengeValue;
	public List<int> challengeSecondaryValue;
	public List<int> challengeReward;
	public List<string> challengeNames;

	//public List<string> currentDescriptions;
	//	public List<int> currentStats;
	//public List<int> currentValues;

	public string allChallenges;

	public List<int> completedChallenges;

	[Header("Menu")]

	public List<Text> challengeMenuText;
	public List<Text> challengeRewardText;
	public List<Image> challengeCompleteImage;

	[Header("Popup")]
	public RectTransform popupBackground;
	public Text pupupChallengeName;
	public Text pupupChallengeText;
	public Text popupReward;
	public Animator popupAnimator;

	public PrizeManager pm;

	CarManager car;

	private void Start()
	{
		instance = this;

		//ParseChallenges();

		//GetCurrentChallenges();

		car = GetComponent<CarManager>();

	}


	public void ParseChallenges (string currentChallenges)
	{
		//Example string
		//!Earn a score of 200.!0!200&Drive until it starts raining.!2!3
		print("current challenges = " + currentChallenges);

		allChallenges = currentChallenges;

		string[] rawChallenges = allChallenges.Split('&');
		string[] components = new string[0];

		//reset all menu text items first so that if there aren't enough, they are not left with old ones.
		foreach (Text item in challengeMenuText)
		{
			item.text = "";
		}

		//reset all menu completion images
		foreach (Image item in challengeCompleteImage)
		{
			item.enabled = false;
		}



		for (int j = 0; j < rawChallenges.Length; j++)
		{
			components = rawChallenges[j].Split('!');
			for (int i = 0; i < components.Length; i++)
			{
				//Description ! challenge stat ! challenge value  ! secondary requirement ! secondary value ! reward ! name
				switch (i)
				{
					case 0:
						challengeDescriptions.Add(components[i]);
						break;
					case 1:
						challengeStat.Add(components[i]);
						break;
					case 2:
						challengeValue.Add(int.Parse(components[i]));
						break;
					case 3:
						challengeSecondary.Add(components[i]);
						break;
					case 4:
						challengeSecondaryValue.Add(int.Parse(components[i]));
						break;
					case 5:
						challengeReward.Add(int.Parse(components[i]));
						break;
					case 6:
						challengeNames.Add(components[i]);
						break;


					default:
						break;
				}

			}
		}

		//get the associated num value of each stat string
		InterperetStatString();

		//get the associated num value of each requirement string
		InterperetRequirementString();

		//update UI with challenges
		UpdateChallengeUI();

		//now that all the lists are filled out, start checking stats
		StartCoroutine(StatCheckLoop());
	}

	void UpdateChallengeUI ()
	{
		if( challengeMenuText.Count > 0)
		{
			//print("number of rewards " + challengeReward.Count);
			for (int i = 0; i < challengeDescriptions.Count; i++) 
			{
				//limit loop to remaining challenges in case there are less that three
				if (i < 3)
				{
					 
					challengeMenuText[i].text = challengeDescriptions[i];

					//check if the reward is money of prize. Reward will be 0 if prize
					print("completed challenge reward is " + challengeReward[i]);
					if (challengeReward[i] > 0)
					{
						challengeRewardText[i].text = "§" + challengeReward[i].ToString();
					}
					else
					{
						challengeRewardText[i].text = "Prize!";
					}
					
				}

			}
		}
	}

	//this is used to intereperet the written form of stat the challenge checks into its int form that is easier to switch
	void InterperetStatString()
	{
		for (int i = 0; i < challengeStat.Count; i++)
		{
			switch (challengeStat[i])
			{
				case "distance":
					challengeStatNum.Add(0);
					break;

				case "level":
					challengeStatNum.Add(1);
					break;

				case "otherside":
					challengeStatNum.Add(2);
					break;

				case "stylePoints":
					challengeStatNum.Add(3);
					break;

				case "hitPotholes":
					challengeStatNum.Add(4);
					break;

				case "hitLightpoles":
					challengeStatNum.Add(5);
					break;

				default:
					break;
			}
		}

		
	}

	//this is used to intereperet the written form of stat the challenge checks into its int form that is easier to switch
	void InterperetRequirementString()
	{
		for (int i = 0; i < challengeSecondary.Count; i++)
		{
			switch (challengeSecondary[i])
			{
				case "none":
					challengeSecondaryNum.Add(0);
					break;

				case "healthMore":
					challengeSecondaryNum.Add(1);
					break;

				case "healthLess":
					challengeSecondaryNum.Add(2);
					break;

				case "distanceMore":
					challengeSecondaryNum.Add(4);
					break;

				case "distanceLess":
					challengeSecondaryNum.Add(5);
					break;

				case "hitPotholesMore":
					challengeSecondaryNum.Add(6);
					break;

				case "hitPotholesLess":
					challengeSecondaryNum.Add(7);
					break;

				case "hitLightpolesMore":
					challengeSecondaryNum.Add(8);
					break;

				case "hitLightpolesLess":
					challengeSecondaryNum.Add(9);
					break;

				case "carType":
					challengeSecondaryNum.Add(10);
					break;

				default:
					break;
			}
		}
	}

	/*void GetCurrentChallenges ()
	{
		for (int i = 0; i < 3; i++)
		{
			currentDescriptions.Add(challengeDescriptions[i]);

			switch (challengeStat[i])
			{
				case "score":
					currentStats.Add(0);
					break;

				case "distance":
					currentStats.Add(1);
					break;

				case "level":
					currentStats.Add(2);
					break;

				case "otherside":
					currentStats.Add(3);
					break;

				case "stylePoints":
					currentStats.Add(4);
					break;

				default:
					break;
			}

			currentValues.Add(challengeValue[i]);
		}
	}*/

	public void ChangeStat(int stat, int val, float dist)
	{
		

		switch (stat)
		{
			case 0:
			distance += dist;
				break;

			case 1:
			level += val;
				break;

			case 2:
			otherside += val;
				break;

			case 3:
			stylePoints += val;
				break;

			case 4:
			hitPotholes += val;
				break;

			case 5:
			hitLightpoles += val;
				break;

			default:
				break;
		}
	}

	IEnumerator StatCheckLoop()
	{
		while (!gameOver)
		{
			CheckStat();

			yield return new WaitForSeconds(1);
		}
	}

	public void CheckStat()
	{
		int numOfStats;
		//check the first three stats in list against the first three desired values in list

		//make sure there are still 3 left, if not, just check the remaining
		if (challengeStatNum.Count >= 3)
		{
			numOfStats = 3;
		}
		else
		{
			numOfStats = challengeStatNum.Count;
		}

		for (int i = 0; i < numOfStats; i++)
		{
			
			switch (challengeStatNum[i])
			{
				case 0:
				if (distance >= challengeValue[i])
					{
						if (challengeSecondaryNum[i] != 0)
						{
							CheckChallangeRequirement(i);
						}
						else
						{
							CompletedChallenge(i);
						}
						
					}
					break;

				case 1:
				if (level >= challengeValue[i])
					{
						if (challengeSecondaryNum[i] != 0)
						{
							CheckChallangeRequirement(i);
						}
						else
						{
							CompletedChallenge(i);
						}
					}
					break;

				case 2:
					if (otherside >= challengeValue[i])
					{
						if (challengeSecondaryNum[i] != 0)
						{
							CheckChallangeRequirement(i);
						}
						else
						{
							CompletedChallenge(i);
						}
					}
					break;

				case 3:
					if (stylePoints >= challengeValue[i])
					{
						if (challengeSecondaryNum[i] != 0)
						{
							CheckChallangeRequirement(i);
						}
						else
						{
							CompletedChallenge(i);
						}
					}
					break;

				case 4:
					if (hitPotholes >= challengeValue[i])
					{
						if (challengeSecondaryNum[i] != 0)
						{
							CheckChallangeRequirement(i);
						}
						else
						{
							CompletedChallenge(i);
						}
					}
					break;

				default:
					break;
			}
		}
	}

	void CheckChallangeRequirement(int challengeIndx)
	{
		switch (challengeSecondaryNum[challengeIndx])
		{
			case 0:
				//do nothing
				
				break;

			case 1:
				//health more than...
				if (car.health > challengeSecondaryValue[challengeIndx])
				{
					//success
					CompletedChallenge(challengeIndx);
				}
				break;

			case 2:
				//health less than...
				if (car.health < challengeSecondaryValue[challengeIndx])
				{
					//success
					CompletedChallenge(challengeIndx);
				}
				break;

			case 3:
				//taken any damage
				if (!tookDamage)
				{
					//success
					CompletedChallenge(challengeIndx);
				}
				break;

			case 4:
				//traveled more than this distance
				if (distance > challengeSecondaryValue[challengeIndx])
				{
					//success
					CompletedChallenge(challengeIndx);
				}
				break;

			case 5:
				//traveled less than this distance
				if (distance < challengeSecondaryValue[challengeIndx])
				{
					//success
					CompletedChallenge(challengeIndx);
				}
				break;

			case 6:
				//hit more than this # of potholes
				if (hitPotholes > challengeSecondaryValue[challengeIndx])
				{
					//success
					CompletedChallenge(challengeIndx);
				}
				break;

			case 7:
				//hit less than this # of potholes
				if (hitPotholes < challengeSecondaryValue[challengeIndx])
				{
					//success
					CompletedChallenge(challengeIndx);
				}
				break;

			case 8:
				//hit more than this # of lightpoles
				if (hitLightpoles > challengeSecondaryValue[challengeIndx])
				{
					//success
					CompletedChallenge(challengeIndx);
				}
				break;

			case 9:
				//hit less than this # of lightpoles
				if (hitLightpoles < challengeSecondaryValue[challengeIndx])
				{
					//success
					CompletedChallenge(challengeIndx);
				}
				break;

			case 10:
				//type of car player is driving
				if (carType == challengeSecondaryValue[challengeIndx])
				{
					//success
					CompletedChallenge(challengeIndx);
				}
				break;

			default:
				break;
		}
	}

	void CompletedChallenge(int challengeIndex)
	{

		if (!completedChallenges.Contains(challengeIndex))
		{
			completedChallenges.Add(challengeIndex);
			print("Completed challenge " + challengeDescriptions[challengeIndex]);

			challengeCompleteImage[challengeIndex].enabled = true;
			
			//update popup
			pupupChallengeText.text = challengeDescriptions[challengeIndex];
			pupupChallengeName.text = challengeNames[challengeIndex];

			print("completed challenge reward is " + challengeReward[challengeIndex]);
			if (challengeReward[challengeIndex] > 0)
			{
				popupReward.text = "+$" + challengeReward[challengeIndex];
			}
			else
			{
				popupReward.text = "Prize!";
			}
			
			AnimatePopup();
		}
		
	} 

	void AnimatePopup()
	{
		popupAnimator.SetTrigger("ChallengeComplete");

	}

	public void GameCompleteStatCheck()
	{
		gameOver = true;
		//do one final check when the game ends in case something was missed during the loop
		CheckStat();
		//add the score for completed challenges once game ends
		RewardCompletedChallenges();

	}

	public void GameOver ()
	{
		
		//clean up challenge list and get new ones
		GetNewChallenges();
		//update the UI with the new challenges
		UpdateChallengeUI();
	}

	void RewardCompletedChallenges()
	{
		print("rewarding should happen " + completedChallenges.Count);
		for (int i = 0; i < completedChallenges.Count; i++) 
		{
			if (challengeReward[completedChallenges[i]] == 0)
			{
				//if the reward is zero, a prize should be rewarded
				pm.GetNextPrize();
			}
			else
			{
				//add needed SP to total
				GameManager.instance.UpdateScore(challengeReward[completedChallenges[i]]);
			}
			
		}
	}

	void GetNewChallenges()
	{
		for (int i = completedChallenges.Count - 1; i >= 0; i--)
		{
			//clean up these lists before saving back to string
			challengeDescriptions.RemoveAt(completedChallenges[i]);
			challengeStat.RemoveAt(completedChallenges[i]);
			challengeStatNum.RemoveAt(completedChallenges[i]);
			challengeValue.RemoveAt(completedChallenges[i]);
			challengeSecondary.RemoveAt(completedChallenges[i]);
			challengeSecondaryNum.RemoveAt(completedChallenges[i]);
			challengeSecondaryValue.RemoveAt(completedChallenges[i]);
			challengeReward.RemoveAt(completedChallenges[i]);
			challengeNames.RemoveAt(completedChallenges[i]);
		}

		//reset all menu completion images
		foreach (Image item in challengeCompleteImage)
		{
			item.enabled = false;
		}

		RebuildPlayerChallengeString();

		
	}

	void RebuildPlayerChallengeString ()
	{
		string remainingPlayerChallenges = "";
		for (int i = 0; i < challengeDescriptions.Count; i++)
		{
			//re-construct the string to be parsed again on load
			//Description ! challenge stat ! challenge value  ! secondary requirement ! secondary value ! reward ! name
			remainingPlayerChallenges += challengeDescriptions[i] + "!" + challengeStat[i] + "!" + challengeValue[i] + "!" + challengeSecondary[i] + "!" + challengeSecondaryValue[i] + "!" + challengeReward[i] + "!" + challengeNames[i];

			if (i != challengeDescriptions.Count - 1)
			{
				remainingPlayerChallenges += "&";
			}
		}

		print(remainingPlayerChallenges);
		PackageForFile(remainingPlayerChallenges);
	}

	public void ResetAllProgress ()
	{
		//deprecated: should be using the one in the file manager instead
		/*
		GameData freshData = new GameData();
		print("MCL = " + freshData.masterChallengeList);
		freshData.playerChallengeList = freshData.masterChallengeList;

		//clear out all existing lists
		challengeDescriptions.Clear();
		challengeStat.Clear();
		challengeStatNum.Clear();
		challengeValue.Clear();

		FileManager.instance.Save(freshData);
		FileManager.instance.Load();*/
	}

	public void PackageForFile(string remainingChallenges)
	{ 
		GameData data = FileManager.instance.currentGameData;

		int curTotalSP = StylePointHandler.instance.readableStylePoints;
		data.stylePoints = curTotalSP;

		//check if the current total SP's is the largest saved amount
		if (curTotalSP > data.maxSavedSP)
		{
			data.maxSavedSP = curTotalSP;
			//thiis is where we will tell the player if new mods become available
		}

		//check if this run's distance was the farthest distance
		print("cur distance is " + distance + " and max distance was " + data.maxDistance);
		if (distance > data.maxDistance)
		{
			data.maxDistance = distance;
			//trigger the new best distance UI
			GameManager.instance.MaxDistanceAchieved();
		}

		data.playerChallengeList = remainingChallenges;

		FileManager.instance.Save(data);
	}
}

