﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Advertisements;


public class AdDisplayer : MonoBehaviour {

	public bool inMenu;
	private int rewardAmount;
	
	[Header("Menu Items")]
	public int rewardFlatRate;
	public GameObject moneyWindow;
	[Header("In-game Items")]
	public float rewardMultiplier;
	public Button adButton;

#if UNITY_IOS
	private string gameId = "1536570";
#elif UNITY_ANDROID
	private string gameId = "1536569";
#endif
    private string gameId = "1536570";  

    void Awake()
	{
		if (!Advertisement.isInitialized)
    	{
       		Advertisement.Initialize(gameId, true);  //// 1st parameter is String and 2nd is boolean
    	}
		
		Advertisement.Initialize(gameId);

		//update menu button Text
		StartCoroutine(UpdateButtonText());

	}

	IEnumerator UpdateButtonText()
	{
		yield return new WaitForSeconds(0.1f);

		if (inMenu) 
		{
			adButton.GetComponentInChildren<Text>().text = "Watch an ad for $" + rewardFlatRate;
		} 
		else {
			adButton.GetComponentInChildren<Text>().text = "Watch Ad for a " + (rewardMultiplier * 100).ToString() + "% Bonus!";
		}


	}

	public void ShowRewardedVideo()
	{

		StartCoroutine(ShowAdWhenReady());
	}

	IEnumerator ShowAdWhenReady()
     {
		while (!Advertisement.IsReady("rewardedVideo"))
             yield return null;

		var options = new ShowOptions();
		options.resultCallback = HandleShowResult;

		print("ad status: " + Advertisement.GetPlacementState());
		Advertisement.Show("rewardedVideo", options);
     }

	void HandleShowResult(ShowResult result)
	{
		if (result == ShowResult.Finished)
		{
			Debug.Log("Video completed - Offer a reward to the player");

			

			if (inMenu)
			{
				rewardAmount = rewardFlatRate;
				Payout(rewardAmount);
				
			}
			else
			{
				GameManager.instance.ToggleMenu(false);

				int curPoints = GameManager.instance.earnedCash;
				int diff = Mathf.RoundToInt(curPoints * rewardMultiplier);
				rewardAmount = diff;
				Payout(rewardAmount);
				//re-show menu when ad is done
				
				GameManager.instance.ToggleMenu(true);
				adButton.interactable = false;
				adButton.GetComponentInChildren<Text>().text = "Available again after next drive.";
			}


		}
		else if (result == ShowResult.Skipped)
		{
			Debug.LogWarning("Video was skipped - Do NOT reward the player");

		}
		else if (result == ShowResult.Failed)
		{
			Debug.LogError("Video failed to show");
		}
	}

	public void Payout(int amount)
	{
		GameData curData = FileManager.instance.currentGameData;
		curData.stylePoints += amount;

		//check if the recently added purchase makes the highest saved cash

		if (curData.stylePoints > curData.maxSavedSP)
		{
			curData.maxSavedSP = curData.stylePoints;
		}

		FileManager.instance.Save(curData);
		FileManager.instance.Load();
		StylePointHandler.instance.SyncLoadedStylePoints();

		if (inMenu)
		{
			MenuRefresh();
		}
	}

	public void MenuRefresh()
	{
		
		GarageManager.instance.AssessItemPrices();
		GarageManager.instance.EvaluatePrice();

		HideWindow();
	}


	public void HideWindow ()
	{
		moneyWindow.SetActive(false);
	}

	public void ShowWindow ()
	{
		moneyWindow.SetActive(true);
	}
}
