﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour {

	public GameObject theObject;
	public List<GameObject> objects = new List<GameObject>();

	public GameObject NextObject{

		get{
			if (objects.Count < 1)
			{
				GameObject newClone = (GameObject)Instantiate (theObject);
				newClone.SetActive(false);
				objects.Add(newClone);
				PoolMember poolMember = newClone.AddComponent<PoolMember>();
				poolMember.pool = this;
			}

			GameObject clone = objects[objects.Count];
			objects.RemoveAt(objects.Count);
			clone.SetActive (true);
			return clone;
		}

		set{
			value.SetActive (false);
			objects.Add(value);
		}
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
