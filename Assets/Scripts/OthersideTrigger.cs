﻿using UnityEngine;

public class OthersideTrigger : MonoBehaviour {

	public CarManager car;

	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "PlayerCar")
		{
			car.otherSide = true;
			GameManager.instance.StartOtherSideBonus();
		}
	}

	void OnTriggerExit(Collider col)
	{
		if (col.gameObject.tag == "PlayerCar")
		{
			car.otherSide = false;
			GameManager.instance.StoptOtherSideBonus();
		}
	}


}
